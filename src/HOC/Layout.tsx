import React, { ReactNode } from "react";
import HeaderSider from "../component/HeaderSider";
interface LayoutProps {
  children: ReactNode;
}

export default function Layout({ children }: LayoutProps) {
  return (
    <div className="flex">
      <HeaderSider />
      <div className="w-full">{children}</div>
    </div>
  );
}
