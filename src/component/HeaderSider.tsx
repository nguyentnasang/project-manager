import React, { useEffect, useRef, useState } from "react";
import { Url_Pb } from "../utils/Utils_Url";
import { NavLink, useNavigate } from "react-router-dom";
import { userLocalService } from "../Service/localService";

export default function HeaderSider() {
  const navigate = useNavigate();
  const [dashBoard, setDashBoard] = useState(false);
  const [device, setDevice] = useState(false);
  const [service, setService] = useState(false);
  const [numLevel, setNumlevel] = useState(false);
  const [report, setReport] = useState(false);
  const [setting, setSetting] = useState(false);
  const [OffNav, setOffNav] = useState(false);
  const changedashBoard = dashBoard ? "bg-orange-700  brightness-200" : "";
  const changedevice = device ? "bg-orange-700  brightness-200" : "";
  const changeservice = service ? "bg-orange-700  brightness-200" : "";
  const changenumLevel = numLevel ? "bg-orange-700  brightness-200" : "";
  const changereport = report ? "bg-orange-700  brightness-200" : "";
  const changesetting = setting ? "bg-orange-700  brightness-200" : "";
  const changeOffNav = OffNav ? "block" : "hidden";

  return (
    <div
      style={{
        width: 230,
        boxShadow: "0px 4px 6px 0px rgba(219, 219, 219, 0.5)",
      }}
      className="relative "
    >
      <div
        onClick={() => {
          setOffNav(false);
        }}
      >
        <h1 className="text-3xl font-bold text-orange-500 mt-10 mb-14">
          SMedi@
        </h1>
        <div
          onClick={() => {
            navigate("/dashboard");
            setDashBoard(true);
            setDevice(false);
            setService(false);
            setNumlevel(false);
            setReport(false);
            setSetting(false);
          }}
          className={`flex py-3 items-center cursor-pointer  ${changedashBoard}`}
        >
          <img
            className="ml-4 mr-3"
            src={Url_Pb + "/img/header/element-4.png"}
            alt=""
          />
          <p className="text-base text-gray-400 font-semibold">Dashboard</p>
        </div>
        <div
          onClick={() => {
            setDashBoard(false);
            setDevice(true);
            setService(false);
            setNumlevel(false);
            setReport(false);
            setSetting(false);
            navigate("/devicemanager");
          }}
          className={`flex py-3 items-center cursor-pointer  ${changedevice}`}
        >
          <img
            className="ml-4 mr-3"
            src={Url_Pb + "/img/header/monitor.png"}
            alt=""
          />
          <p className="text-base text-gray-400 font-semibold">Thiết bị</p>
        </div>
        <div
          onClick={() => {
            navigate("/servicemanager");
            setDashBoard(false);
            setDevice(false);
            setService(true);
            setNumlevel(false);
            setReport(false);
            setSetting(false);
          }}
          className={`flex py-3 items-center cursor-pointer  ${changeservice}`}
        >
          <img
            className="ml-4 mr-3"
            src={Url_Pb + "/img/header/Frame 332.png"}
            alt=""
          />
          <p className="text-base text-gray-400 font-semibold">Dịch vụ</p>
        </div>
        <div
          onClick={() => {
            navigate("/serialmanager");
            setDashBoard(false);
            setDevice(false);
            setService(false);
            setNumlevel(true);
            setReport(false);
            setSetting(false);
          }}
          className={`flex py-3 items-center cursor-pointer  ${changenumLevel}`}
        >
          <img
            className="ml-4 mr-3"
            src={Url_Pb + "/img/header/icon dasboard03.png"}
            alt=""
          />
          <p className="text-base text-gray-400 font-semibold">Cấp số</p>
        </div>
        <div
          onClick={() => {
            navigate("/reportmanager");
            setDashBoard(false);
            setDevice(false);
            setService(false);
            setNumlevel(false);
            setReport(true);
            setSetting(false);
          }}
          className={`flex py-3 items-center cursor-pointer  ${changereport}`}
        >
          <img
            className="ml-4 mr-3"
            src={Url_Pb + "/img/header/Frame (1).png"}
            alt=""
          />
          <p className="text-base text-gray-400 font-semibold">Báo cáo</p>
        </div>
      </div>
      <div className="relative  ">
        <div
          onClick={() => {
            setOffNav(!OffNav);
          }}
          className={`transition-all duration-500  flex py-3 items-center cursor-pointer  relative ${changesetting}`}
        >
          <img
            className="ml-4 mr-3"
            src={Url_Pb + "/img/header/setting.png"}
            alt=""
          />
          <p className="text-base text-gray-400 font-semibold">
            Cài đặt hệ thống
          </p>
          <img
            className="ml-4 mr-3 absolute right-0 bottom-3"
            src={Url_Pb + "/img/header/fi_more-vertical.png"}
            alt=""
          />
        </div>
        <ul
          onClick={() => {
            setDashBoard(false);
            setDevice(false);
            setService(false);
            setNumlevel(false);
            setReport(false);
            setSetting(true);
            setOffNav(!OffNav);
          }}
          style={{
            right: "-208px",
            boxShadow: "3px 3px 4px 0px rgba(70, 64, 67, 0.1)",
          }}
          className={`z-10 font-bold text-gray-400 absolute top-0 bg-white w-52 text-left rounded-r-xl text-base overflow-hidden ${changeOffNav}`}
        >
          <NavLink to={"/rolemanager"}>
            <li className="py-3 pl-4 hover:bg-orange-400 hover:text-white">
              Quản lý vai trò
            </li>
          </NavLink>

          <NavLink to={"/accountmanager"}>
            <li className="py-3 pl-4 hover:bg-orange-400 hover:text-white">
              Quản lý tài khoản
            </li>
          </NavLink>
          <NavLink to={"/historyactivity"}>
            <li className="py-3 pl-4 hover:bg-orange-400 hover:text-white">
              Nhật ký người dùng
            </li>
          </NavLink>
        </ul>
      </div>
      <div
        className=" pt-72 pb-6"
        onClick={() => {
          setOffNav(false);
        }}
      >
        <button
          onClick={() => {
            userLocalService.remove();
            navigate("/");
          }}
          className="flex items-center bg-orange-50 py-3 pl-3 pr-14 mx-auto rounded-lg "
        >
          <img
            className="mr-3"
            src={Url_Pb + "/img/header/fi_log-out.png"}
            alt=""
          />
          <p className="text-orange-500 ">Đăng xuất</p>
        </button>
      </div>
    </div>
  );
}
