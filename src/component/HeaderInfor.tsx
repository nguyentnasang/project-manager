import React, { useEffect, useMemo, useState } from "react";
import { Url_Pb } from "../utils/Utils_Url";
import { NavLink } from "react-router-dom";
import { UserSliceState } from "../redux-toolkit/userSlice";
import { useSelector } from "react-redux";
import { Button, Popover } from "antd";
import firebase from "firebase/compat/app";
import { firebaseConfig } from "../firebase/firebase";

export default function HeaderInfor() {
  // interface RootState {
  //   userSlice: UserSliceState;
  // }

  const userLogin = useSelector((state: any) => state.userSlice.userLogin);
  const text = <span>Title</span>;

  const [showArrow, setShowArrow] = useState(true);
  const [arrowAtCenter, setArrowAtCenter] = useState(false);
  const [dataRender, setDataRender] = useState(false);
  console.log("dataRender", dataRender);

  const mergedArrow = useMemo(() => {
    if (arrowAtCenter) return { pointAtCenter: true };
    return showArrow;
  }, [showArrow, arrowAtCenter]);

  firebase.initializeApp(firebaseConfig);
  const db = firebase.database();
  useEffect(() => {
    db.ref("notifys").once("value", (snapshot) => {
      setDataRender(snapshot.val());
    });
  }, []);

  const RenderNotify = () => {
    const arr = Object.values(dataRender);
    return arr.map((item) => {
      const dataTime = new Date(item.dataTime).toLocaleString("vi-VN");
      return (
        <div
          style={{ borderBottom: "1.5px solid #D4D4D7" }}
          className=" ml-6 mr-10 py-3"
        >
          <p className="mb-1 font-bold text-base text-orange-600">
            Người dùng: {item.fullName}
          </p>
          <p className="text-base font text-gray-400">
            Thời gian nhận số: {dataTime}
          </p>
        </div>
      );
    });
  };
  const content = (
    <div
      style={{
        width: "360px",
        height: "526px",
        marginRight: "-20px",
      }}
      className="overflow-y-auto "
    >
      <div className="bg-orange-400 text-white font-bold text-xl py-2 pl-6">
        Thông báo
      </div>
      {RenderNotify()}
    </div>
  );
  return (
    <NavLink to={"/infor"}>
      <div className="flex mr-16 items-center">
        <Popover
          className="sangggggg"
          placement="bottom"
          content={content}
          arrow={mergedArrow}
        >
          <img
            className="mr-6 max-h-8 max-w-full cursor-pointer"
            src={Url_Pb + "/img/header/Frame 271.png"}
            alt=""
          />
        </Popover>
        <img
          className="max-h-10 mr-2 cursor-pointer"
          src={Url_Pb + "/img/header/unsplash_Fyl8sMC2j2Q.png"}
          alt=""
        />
        <span className="text-left cursor-pointer">
          <p className="text-xs text-gray-400">Xin chào</p>
          <p className="text-base text-gray-500 font-bold">
            {userLogin.fullName}
          </p>
        </span>
      </div>
    </NavLink>
  );
}
