export const USERLOCAL = "USERLOCAL";
export const userLocalService = {
  get: () => {
    let userJson = localStorage.getItem(USERLOCAL);
    if (userJson) {
      return JSON.parse(userJson);
    } else {
      return null;
    }
  },
  set: (data: any) => {
    let userJson = JSON.stringify(data);
    localStorage.setItem(USERLOCAL, userJson);
  },
  remove: () => {
    localStorage.removeItem(USERLOCAL);
  },
};
