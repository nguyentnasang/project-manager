// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import firebase from "firebase/compat/app";
import { useEffect, useState } from "react";

const firebaseConfig = {
  apiKey: "AIzaSyAB_R16GOWQDBHq8eOjpGS9YWtJgmTf8Y8",
  authDomain: "projectmanager-65556.firebaseapp.com",
  databaseURL:
    "https://projectmanager-65556-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "projectmanager-65556",
  storageBucket: "projectmanager-65556.appspot.com",
  messagingSenderId: "222873550957",
  appId: "1:222873550957:web:a77bd9107065b266a7ff64",
  measurementId: "G-VDYJ44P30K",
};

const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
// console.log("Firebase App", app);
export { app, analytics, firebaseConfig };
