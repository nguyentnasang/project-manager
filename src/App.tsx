import React from "react";
import { Route, Routes } from "react-router";
import { BrowserRouter } from "react-router-dom";
import "./App.css";
import Layout from "./HOC/Layout";
import ForgotPassword from "./Pages/ForgotPassword/ForgotPassword";
import InforPersonal from "./Pages/InforPersonal/InforPersonal";
import Login from "./Pages/Login/Login";
import ResetPassword from "./Pages/ResetPassword/ResetPassword";
import AddAcount from "./Pages/AddAccount/AddAcount";
import AccountManager from "./Pages/AccountManager/AccountManager";
import UpdateAccount from "./Pages/UpdateAccount/UpdateAccount";
import AddRole from "./Pages/AddRole/AddRole";
import RoleManager from "./Pages/RoleManager/RoleManager";
import UpdateRole from "./Pages/UpdateRole/UpdateRole";
import AddDevice from "./Pages/AddDevice/AddDevice";
import DeviceManager from "./Pages/DeviceManager/DeviceManager";
import UpdateDevice from "./Pages/UpdateDevice/UpdateDevice";
import DetailDevice from "./Pages/DetailDevice/DetailDevice";
import AddService from "./Pages/AddService/AddService";
import ServiceManager from "./Pages/ServiceManager/ServiceManager";
import UpdateService from "./Pages/UpdateService/UpdateService";
import NumGiven from "./Pages/NumGiven/NumGiven";
import SerialManager from "./Pages/SerialManager/SerialManager";
import DetailSerial from "./Pages/DetailSerial/DetailSerial";
import ReportManager from "./Pages/ReportManager/ReportManager";
import HistoryActivity from "./Pages/HistoryActivity/HistoryActivity";
import Dashboard from "./Pages/Dashboard/Dashboard";
import DetailService from "./Pages/DetailService/DetailService";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/login/forgot" element={<ForgotPassword />} />
          <Route path="/login/forgot/reset" element={<ResetPassword />} />
          <Route
            path="/accountmanager/add"
            element={
              <Layout>
                <AddAcount />
              </Layout>
            }
          />

          <Route
            path="/infor"
            element={
              <Layout>
                <InforPersonal />
              </Layout>
            }
          />
          <Route
            path="/accountmanager"
            element={
              <Layout>
                <AccountManager />
              </Layout>
            }
          />
          <Route
            path="/accountmanager/update/:id"
            element={
              <Layout>
                <UpdateAccount />
              </Layout>
            }
          />
          <Route
            path="/rolemanager/add"
            element={
              <Layout>
                <AddRole />
              </Layout>
            }
          />
          <Route
            path="/rolemanager"
            element={
              <Layout>
                <RoleManager />
              </Layout>
            }
          />
          <Route
            path="/rolemanager/update/:id"
            element={
              <Layout>
                <UpdateRole />
              </Layout>
            }
          />
          <Route
            path="/devicemanager/add"
            element={
              <Layout>
                <AddDevice />
              </Layout>
            }
          />
          <Route
            path="/devicemanager"
            element={
              <Layout>
                <DeviceManager />
              </Layout>
            }
          />
          <Route
            path="/devicemanager/update/:id"
            element={
              <Layout>
                <UpdateDevice />
              </Layout>
            }
          />
          <Route
            path="/devicemanager/detail/:id"
            element={
              <Layout>
                <DetailDevice />
              </Layout>
            }
          />
          <Route
            path="/servicemanager/add"
            element={
              <Layout>
                <AddService />
              </Layout>
            }
          />
          <Route
            path="/servicemanager"
            element={
              <Layout>
                <ServiceManager />
              </Layout>
            }
          />
          <Route
            path="/servivemanager/update/:id"
            element={
              <Layout>
                <UpdateService />
              </Layout>
            }
          />
          <Route
            path="/serialmanager/addserial"
            element={
              <Layout>
                <NumGiven />
              </Layout>
            }
          />
          <Route
            path="/serialmanager"
            element={
              <Layout>
                <SerialManager />
              </Layout>
            }
          />
          <Route
            path="/serialmanager/detail/:id"
            element={
              <Layout>
                <DetailSerial />
              </Layout>
            }
          />
          <Route
            path="/reportmanager"
            element={
              <Layout>
                <ReportManager />
              </Layout>
            }
          />
          <Route
            path="/historyactivity"
            element={
              <Layout>
                <HistoryActivity />
              </Layout>
            }
          />
          <Route
            path="/dashboard"
            element={
              <Layout>
                <Dashboard />
              </Layout>
            }
          />
          <Route
            path="/servicemanager/detail/:id"
            element={
              <Layout>
                <DetailService />
              </Layout>
            }
          />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
