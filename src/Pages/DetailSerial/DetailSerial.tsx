import React, { useEffect, useState } from "react";
import HeaderInfor from "../../component/HeaderInfor";
import { useNavigate, useParams } from "react-router";
import firebase from "firebase/compat/app";
import { firebaseConfig } from "../../firebase/firebase";
import { Url_Pb } from "../../utils/Utils_Url";
interface Serial {
  dateTime: string;
  email: string;
  expiry: string;
  fullName: string;
  nameService: string;
  origin: string;
  phoneNumber: string;
  serial: number;
  status: string;
}
export default function DetailSerial() {
  let navigate = useNavigate();
  let param = useParams().id;
  let [dataSerial, setDataSerial] = useState<Serial>();
  const dateTime = dataSerial
    ? new Date(dataSerial.dateTime).toLocaleString("vi-VN")
    : "";
  const expiry = dataSerial
    ? new Date(dataSerial.expiry).toLocaleString("vi-VN")
    : "";
  console.log("dataSerial", dataSerial);
  firebase.initializeApp(firebaseConfig);
  const db = firebase.database();
  useEffect(() => {
    db.ref(`serials/${param}`)
      .once("value")
      .then((snapshot) => {
        setDataSerial(snapshot.val());
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div className="bg-gray-100 h-full relative">
      <header className="flex justify-between pt-7 ml-7">
        <div className="flex text-xl text-gray-500 font-bold">
          <p className=" cursor-pointer mr-4">Thiết bị</p>
          <p>{">"}</p>
          <p className=" cursor-pointer mr-4 ml-4">Danh sách cấp số</p>
          <p>{">"}</p>
          <p className=" cursor-pointer text-orange-500 ml-4">Chi tiết</p>
        </div>
        <div>
          <HeaderInfor />
        </div>
      </header>
      <div className="text-left mt-9 ml-6 mb-3 ">
        <span className="text-orange-500 text-2xl font-bold ">
          Quản lý cấp số
        </span>
        <div className="mr-24 bg-white rounded-lg mt-6 pt-4 pl-6 pb-80">
          <p className="text-orange-500 text-xl font-bold">Thông tin cấp số</p>
          <div className="grid grid-cols-2">
            <div className="flex">
              <div>
                <p className="mt-4 font-semibold">Họ tên: </p>
                <p className="mt-4 font-semibold">Tên dịch vụ:</p>
                <p className="mt-4 font-semibold">Số thứ tự:</p>
                <p className="mt-4 font-semibold">Thời gian cấp:</p>
                <p className="mt-4 font-semibold">Hạn sử dụng:</p>
              </div>
              <div className="ml-6">
                <p className="mt-4">{dataSerial?.fullName}</p>
                <p className="mt-4">{dataSerial?.nameService}</p>
                <p className="mt-4">{dataSerial?.serial}</p>
                <p className="mt-4">{dateTime}</p>
                <p className="mt-4">{expiry}</p>
              </div>
            </div>
            <div className="flex">
              <div>
                <p className="mt-4 font-semibold">Nguồn cấp:</p>
                <p className="mt-4 font-semibold">Trạng thái:</p>
                <p className="mt-4 font-semibold">Số điện thoại:</p>
                <p className="mt-4 font-semibold">Địa chỉ Email:</p>
              </div>
              <div className="ml-6">
                <p className="mt-4">{dataSerial?.origin}</p>
                <div className="">
                  <div className="mt-4 flex items-center">
                    <div className="bg-blue-500 w-2 h-2 rounded-full mr-1"></div>
                    <p>{dataSerial?.status}</p>
                  </div>
                </div>
                <p className="mt-4">{dataSerial?.phoneNumber}</p>
                <p className="mt-4">{dataSerial?.email}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <img
        onClick={() => {
          navigate("/serialmanager");
        }}
        className="absolute top-40 right-0"
        src={Url_Pb + "/img/servicemanager/Component 1.png"}
        alt=""
      />
    </div>
  );
}
