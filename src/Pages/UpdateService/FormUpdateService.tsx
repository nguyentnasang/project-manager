import { Button, Checkbox, Form, Input, message } from "antd";
import TextArea from "antd/es/input/TextArea";
import React, { useEffect, useState } from "react";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import firebase from "firebase/compat/app";
import { firebaseConfig } from "../../firebase/firebase";
import { useSelector } from "react-redux";
interface Service {
  codeService: string;
  nameService: string;
  descService: string;
  numberRule: any;
}
export default function FormUpdateService() {
  firebase.initializeApp(firebaseConfig);
  const userLogin = useSelector((state: any) => state.userSlice.userLogin);

  const db = firebase.database();
  const navigate = useNavigate();
  const param = useParams().id;
  const [dataService, setDataService] = useState<Service>();
  console.log("dataService", dataService);
  const onFinish = async (values: any) => {
    db.ref(`services/${param}`).update(values);
    message.success("cập nhật dịch vụ thành công");
    db.ref("activities").push({
      userName: userLogin.fullName,
      dateTime: `${new Date()}`,
      IP: values.codeService,
      operation: "cập nhật dịch vụ",
    });
    navigate("/servicemanager");
  };
  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };
  useEffect(() => {
    db.ref(`services/${param}`).once("value", (snapshot) => {
      const serviceData = snapshot.val();
      setDataService(serviceData);
    });
  }, []);

  return (
    <div className="mt-8">
      <Form
        fields={[
          {
            name: ["codeService"],
            value: dataService?.codeService,
          },
          {
            name: ["nameService"],
            value: dataService?.nameService,
          },
          {
            name: ["descService"],
            value: dataService?.descService,
          },

          {
            name: ["numberRule"],
            value: dataService?.numberRule,
          },
        ]}
        layout="vertical"
        name="basic"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <div className="bg-white pt-4 pl-6 pb-3 rounded-2xl">
          <p className="text-orange-500 text-xl font-bold mb-3">
            Thông tin dịch vụ
          </p>
          <div className="grid grid-cols-2 gap-6">
            <div>
              <Form.Item
                label={
                  <div className="flex">
                    <p className="mr-1 font-semibold">Mã dịch vụ: </p>
                    <p className="text-red-500 font-bold">*</p>
                  </div>
                }
                name="codeService"
                rules={[
                  { required: true, message: "Vui lòng nhập mã dịch vụ!" },
                ]}
              >
                <Input placeholder="Nhập mã dịch vụ" />
              </Form.Item>

              <Form.Item
                label={
                  <div className="flex">
                    <p className="mr-1 font-semibold">Tên dịch vụ : </p>
                    <p className="text-red-500 font-bold">*</p>
                  </div>
                }
                name="nameService"
                rules={[
                  { required: true, message: "Vui lòng nhập tên dịch vụ!" },
                ]}
              >
                <Input placeholder="Nhập tên dịch vụ" />
              </Form.Item>
            </div>
            <Form.Item
              label={<p className="mr-1 font-semibold">Mô tả:</p>}
              name="descService"
            >
              <TextArea rows={5} placeholder="Nhập mô tả" />
            </Form.Item>
          </div>
          <div>
            <p className="text-orange-500 text-xl font-bold mb-3">
              Quy tắc cấp số
            </p>

            <Form.Item name="numberRule">
              <Checkbox.Group className="grid grid-cols-8">
                <div className="flex flex-col col-span-1">
                  <Checkbox className="font-semibold mt-1" value="auto">
                    Tăng tự động từ:
                  </Checkbox>

                  <Checkbox
                    className="font-semibold py-6"
                    value="prefix"
                    style={{ lineHeight: "32px" }}
                  >
                    Prefix:
                  </Checkbox>

                  <Checkbox
                    className="font-semibold pb-6"
                    value="surfix"
                    style={{ lineHeight: "32px" }}
                  >
                    Surfix:
                  </Checkbox>

                  <Checkbox
                    className="font-semibold"
                    value="reset"
                    style={{ lineHeight: "32px" }}
                  >
                    Reset mỗi ngày
                  </Checkbox>
                </div>

                <div className="">
                  <div className="flex items-center">
                    <p className="rounded-lg border-2 border-gray-200 py-1 px-3">
                      0001
                    </p>
                    <span className="mx-3">đến</span>
                    <p className="rounded-lg border-2 border-gray-200 py-1 px-3">
                      9999
                    </p>
                  </div>
                  <div className="flex my-5">
                    <p className="rounded-lg border-2 border-gray-200 py-1 px-3">
                      0001
                    </p>
                  </div>
                  <div className="flex">
                    <p className="rounded-lg border-2 border-gray-200 py-1 px-3">
                      0001
                    </p>
                  </div>
                </div>
              </Checkbox.Group>
            </Form.Item>
            <span className="flex items-center mt-4 text-sm font-normal ">
              <p className="text-red-500 ml-1 mr-1">*</p>
              <p className="text-gray-400">Là trường thông tin bắt buộc</p>
            </span>
          </div>
        </div>
        <Form.Item>
          <div className="flex text-center justify-center mt-6">
            <NavLink to={"/servicemanager"}>
              <button className="bg-orange-50 border-2 border-orange-400 text-orange-400 px-14 py-3 rounded-lg mr-8">
                Hủy bỏ
              </button>
            </NavLink>
            <Button
              className="bg-orange-500 text-white px-10 py-6 rounded-lg flex items-center"
              htmlType="submit"
            >
              Cập nhật
            </Button>
          </div>
        </Form.Item>
      </Form>
    </div>
  );
}
