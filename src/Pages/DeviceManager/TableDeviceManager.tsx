import { Table, Tooltip } from "antd";
import React, { useEffect, useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import { Url_Pb } from "../../utils/Utils_Url";
import { ColumnsType } from "antd/es/table";
import DeviceManager from "./DeviceManager";
import firebase from "firebase/compat/app";
import { firebaseConfig } from "../../firebase/firebase";
interface Device {
  codeDevice: string;
  nameDevice: string;
  IP: string;
  status: string;
  connec: string;
  useDevice: any;
  action: any;
  detail: any;
}
export default function TableDeviceManager() {
  const navigate = useNavigate();
  const [dataDevice, setDataDevice] = useState<Device[]>([]);
  const [fullString, setFullString] = useState("");
  console.log("fullString", fullString);
  const columns: ColumnsType<Device> = [
    {
      title: "Mã thiết bị",
      dataIndex: "codeDevice",
      key: "codeDevice",
      render: (text: any) => <a className="">{text}</a>,
    },
    {
      title: "Tên thiết bị",
      dataIndex: "nameDevice",
      key: "nameDevice",
    },
    {
      title: "Địa chỉ IP",
      dataIndex: "IP",
      key: "IP",
    },
    {
      title: "Trạng thái hoạt động",
      dataIndex: "status",
      key: "status",
      render: (text: any) => {
        if (text == "hoatdong") {
          return (
            <div className="flex items-center">
              <div className="bg-green-500 w-2 h-2 rounded-full mr-1"></div>
              <p>Hoạt động</p>
            </div>
          );
        } else if (text == "ngunghoatdong") {
          return (
            <div className="flex items-center">
              <div className="bg-red-500 w-2 h-2 rounded-full mr-1"></div>
              <p>Ngưng hoạt động</p>
            </div>
          );
        }
      },
    },
    {
      title: "Trạng thái kết nối",
      dataIndex: "connec",
      key: "connec",
      render: (text: any) => {
        if (text == "ketnoi") {
          return (
            <div className="flex items-center">
              <div className="bg-green-500 w-2 h-2 rounded-full mr-1"></div>
              <p>Kết nối</p>
            </div>
          );
        } else if (text == "ngungketnoi") {
          return (
            <div className="flex items-center">
              <div className="bg-red-500 w-2 h-2 rounded-full mr-1"></div>
              <p>Ngưng kết nối</p>
            </div>
          );
        }
      },
    },
    {
      title: "Dịch vụ sử dụng",
      dataIndex: "useDevice",
      key: "useDevice",
      ellipsis: {
        showTitle: true,
      },
      render: (address) => (
        <Tooltip placement="topLeft" title={address}>
          {address}
          <p
            onClick={() => {}}
            className="text-blue-500 underline cursor-pointer"
          >
            Xem thêm
          </p>
        </Tooltip>
      ),
    },

    {
      title: " ",
      dataIndex: "detail",
      key: "detail",
    },
    {
      title: " ",
      dataIndex: "action",
      key: "action",
    },
  ];
  firebase.initializeApp(firebaseConfig);
  const db = firebase.database();
  const deviceRef = db.ref("devices");
  // if (!dataDevice) {
  //   return []; // Trả về một mảng rỗng nếu dataDevice là undefined hoặc null
  // }
  useEffect(() => {
    deviceRef.once("value", (snapshot) => {
      setDataDevice(snapshot.val());
    });
  }, []);
  const renderDevice = (): Device[] => {
    const arr = Object.values(dataDevice) as Device[];
    const arrDevice = arr?.map((item, index) => {
      //lấy key của obj để truyền vào cập nhật
      const ObjKey: string = Object.keys(dataDevice)[index];
      return {
        ...item,
        status: "hoatdong",
        connec: "ketnoi",
        useDevice: item.useDevice.map(
          (item: any, index: number, array: any[]) => {
            if (index === array.length - 1) {
              // kiểm tra phần tử cuối cùng trong mảng
              return `${item}.`; // thay dấu phẩy bằng dấu chấm
            }
            return `${item}, `;
          }
        ),
        detail: (
          <p
            onClick={() => {
              navigate(`/devicemanager/detail/${ObjKey}`);
            }}
            className="text-blue-500 underline cursor-pointer"
          >
            Chi tiết
          </p>
        ),
        action: (
          <p
            onClick={() => {
              navigate(`/devicemanager/update/${ObjKey}`);
            }}
            className="text-blue-500 underline cursor-pointer"
          >
            Cập nhật
          </p>
        ),
      };
    });
    console.log("arr", arr);

    return arrDevice;
  };

  return (
    <div>
      <div className="ml-6 mt-11 pr-28">
        <p className="text-orange-500 text-2xl font-bold text-left mb-4">
          Danh sách thiết bị
        </p>
        <div className="flex justify-between mb-4">
          <div className="flex gap-6">
            <div>
              <p className="text-left text-base font-semibold mb-1">
                Trạng thái hoạt động
              </p>
              <select
                style={{ width: 300 }}
                className="py-1 border-2 border-gray-100 rounded-lg pl-3"
              >
                <option value="" className="hover:bg-orange-400">
                  Tất cả
                </option>
                <option value="ngunghoatdong" className="hover:bg-orange-400">
                  Ngưng hoạt động
                </option>
                <option value="hoatdong" className="hover:bg-orange-400">
                  Hoạt động
                </option>
              </select>
            </div>
            <div>
              <p className="text-left text-base font-semibold mb-1">
                Trạng thái kết nối
              </p>
              <select
                style={{ width: 300 }}
                className="py-1 border-2 border-gray-100 rounded-lg pl-3"
              >
                <option value="" className="hover:bg-orange-400">
                  Tất cả
                </option>
                <option value="ketnoi" className="hover:bg-orange-400">
                  Kết nối
                </option>
                <option value="matketnoi" className="hover:bg-orange-400">
                  Mất kết nối
                </option>
              </select>
            </div>
          </div>
          <div className="relative">
            <p className="text-left text-base font-semibold mb-1">Từ khoá</p>
            <input
              placeholder="Nhập từ khóa"
              className="py-1 border-2 border-gray-100 rounded-lg pl-3 w-72"
              type="text"
            />
            <img
              className="absolute right-3 top-9"
              src={Url_Pb + "/img/accountmanager/fi_search.png"}
              alt=""
            />
          </div>
        </div>
        <Table className="" columns={columns} dataSource={renderDevice()} />
        {/* rederUserData()  */}
        <NavLink to={"/devicemanager/add"}>
          <img
            style={{ top: 235 }}
            className="absolute  right-0 cursor-pointer"
            src={Url_Pb + "/img/devicemanager/Component 1.png"}
            alt=""
          />
        </NavLink>
      </div>
    </div>
  );
}
