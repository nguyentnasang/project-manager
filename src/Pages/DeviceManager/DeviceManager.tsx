import React from "react";
import HeaderInfor from "../../component/HeaderInfor";
import TableDeviceManager from "./TableDeviceManager";

export default function DeviceManager() {
  return (
    <div>
      <header className="flex justify-between pt-7 ml-7">
        <div className="flex text-xl text-gray-500 font-bold">
          <p className=" cursor-pointer mr-4">Thiết bị</p>
          <p>{">"}</p>

          <p className=" cursor-pointer text-orange-500 ml-4">
            Danh sách thiết bị
          </p>
        </div>
        <div>
          <HeaderInfor />
        </div>
      </header>
      <TableDeviceManager />
    </div>
  );
}
