import React, { useEffect, useState } from "react";
import HeaderInfor from "../../component/HeaderInfor";
import { Button, Form, Input, Select, message, Modal } from "antd";
import { Option } from "antd/es/mentions";
import { NavLink } from "react-router-dom";
import firebase from "firebase/compat/app";
import { firebaseConfig } from "../../firebase/firebase";
import { useSelector } from "react-redux";
import "firebase/firestore";

export default function NumGiven() {
  const user = useSelector((state: any) => state.userSlice.userLogin);
  const [dataService, setDataService] = useState<any>([]);
  const [serialOlw, setSerialOlw] = useState<any>({ serial: 0 });
  const [serialNext, setSerialNext] = useState<any>();
  console.log("serialNext", serialNext);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [dataSeri, setDataSeri] = useState<any>({});
  const dateTime = new Date(dataSeri.dateTime);
  const formattedDateTime = `${dateTime.toLocaleTimeString()} ${dateTime.toLocaleDateString()}`;
  const expiry = new Date(dataSeri.expiry);
  const formattedExpiry = `${expiry.toLocaleTimeString()} ${expiry.toLocaleDateString()}`;
  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };
  useEffect(() => {
    const firstValue: any = Object.values(serialOlw)[0];

    if (firstValue) {
      setSerialNext(firstValue.serial);
    }
  }, [serialOlw]);

  const onFinish = async (values: any) => {
    console.log(values);
    const currentDate = new Date();
    currentDate.setHours(currentDate.getHours() + 3);

    const data: any = {
      nameService: values.nameService,
      fullName: user.fullName,
      phoneNumber: user.phoneNumber,
      email: user.email,
      status: "Đang chờ",
      origin: "Kiosk",
      serial: serialNext + 1,
      dateTime: `${new Date()}`,
      expiry: `${currentDate}`,
    };
    setSerialNext(serialNext + 1);
    setDataSeri(data);
    db.ref("serials")
      .push(data)
      .then((res) => {
        console.log(res);
        message.success("thêm thành công");
      })
      .catch((err) => {
        console.log(err);
      });
    const datanew = { fullName: data.fullName, dataTime: data.dataTime };
    console.log("datanew", datanew);
    db.ref("notifys")
      .push({ fullName: data.fullName, dataTime: `${new Date()}` })
      .then((res) => {
        console.log(res);
        message.success("đã lưu ở thông báo");
      })
      .catch((err) => {
        console.log(err);
      });
    console.log("data", data);
  };
  const onFinishFailed = (errorInfo: any) => {
    console.log("errorInfo", errorInfo);
  };
  firebase.initializeApp(firebaseConfig);
  const db = firebase.database();
  useEffect(() => {
    db.ref("services")
      .orderByChild("nameService")
      .once("value", (snapshot) => {
        const dataServiceSnapshot = snapshot.val();
        setDataService(dataServiceSnapshot);
      });
    db.ref("serials")
      .limitToLast(1)
      .once("value")
      .then((snapshot) => {
        setSerialOlw(snapshot.val());
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const renderOption = () => {
    const arrServices = Object.values(dataService);
    return arrServices.map((item: any) => {
      return (
        <Select.Option key={item.codeService} value={item.nameService}>
          {item.nameService}
        </Select.Option>
      );
    });
  };

  return (
    <div className="bg-gray-100 h-full">
      <header className="flex justify-between pt-7 ml-7">
        <div className="flex text-xl text-gray-500 font-bold">
          <p className=" cursor-pointer mr-4">Cấp số</p>
          <p>{">"}</p>
          <p className=" cursor-pointer mr-4 ml-4">Danh sách cấp số</p>
          <p>{">"}</p>
          <p className=" cursor-pointer text-orange-500 ml-4">Cấp số mới</p>
        </div>
        <div>
          <HeaderInfor />
        </div>
      </header>
      <div className="text-left mt-9 ml-6 mb-3">
        <span className="text-orange-500 text-2xl font-bold ">
          Quản lý cấp số
        </span>
        <div className="mr-6 flex flex-col justify-center items-center bg-white rounded-lg pt-6 pb-96">
          <p className="text-3xl font-bold text-orange-500 mb-5">CẤP SỐ MỚI</p>
          <p className="font-bold mb-3">Dịch vụ khách hàng lựa chọn</p>
          <Form
            name="control-hooks"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
          >
            <Form.Item
              name="nameService"
              rules={[{ required: true, message: "Vui lòng chọn dịch vụ!" }]}
            >
              <Select placeholder="Chọn dịch vụ" allowClear>
                {renderOption()}
              </Select>
            </Form.Item>
            <Form.Item
              noStyle
              shouldUpdate={(prevValues, currentValues) =>
                prevValues.gender !== currentValues.gender
              }
            >
              {({ getFieldValue }) =>
                getFieldValue("gender") === "other" ? (
                  <Form.Item
                    name="customizeGender"
                    label="Customize Gender"
                    rules={[{ required: true }]}
                  >
                    <Input />
                  </Form.Item>
                ) : null
              }
            </Form.Item>
            <Form.Item>
              <div className="flex text-center justify-center mt-6">
                <NavLink to={"/serialmanager"}>
                  <button className="bg-orange-50 border-2 border-orange-400 text-orange-400 px-14 py-3 rounded-lg mr-8">
                    Hủy bỏ
                  </button>
                </NavLink>
                <Button
                  className="bg-orange-500 text-white px-16 py-6 rounded-lg flex items-center"
                  htmlType="submit"
                  onClick={showModal}
                >
                  In số
                </Button>
                <Modal
                  width={469}
                  open={isModalOpen}
                  onOk={handleOk}
                  onCancel={handleCancel}
                  footer={[
                    <Button
                      className="hidden"
                      key="submit"
                      type="primary"
                      onClick={handleOk}
                    >
                      OKE
                    </Button>,
                    <Button
                      className="hidden"
                      key="cancel"
                      onClick={handleCancel}
                    >
                      CANCEL
                    </Button>,
                  ]}
                >
                  <div className="flex flex-col justify-center">
                    <p className="m-auto font-bold text-2xl mt-5 text-gray-400">
                      Số thứ tự được cấp
                    </p>
                    <p className="m-auto text-5xl font-extrabold text-orange-500 mt-6">
                      {dataSeri.serial}
                    </p>
                    <p className="m-auto mt-6 mb-11 text-lg">
                      {dataSeri.nameService
                        ? `DV: ${dataSeri.nameService} (tại quầy số 1)`
                        : "vui lòng chọn dịch vụ trước khi in số"}
                    </p>
                  </div>
                  <div className="bg-orange-400 w-full flex flex-col justify-center py-4 text-base text-white">
                    <p className="m-auto ">{`Thời gian cấp: ${formattedDateTime}`}</p>
                    <p className="m-auto">{`Hạn sử dụng: ${formattedExpiry}`}</p>
                  </div>
                </Modal>
              </div>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}
