import React, { useEffect, useState } from "react";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import firebase from "firebase/compat/app";
import { firebaseConfig } from "../../firebase/firebase";
import { message } from "antd";
interface Role {
  nameRole: string;
  describe: string;
  isCheckedA1: boolean;
  isCheckedA2: boolean;
  isCheckedA3: boolean;
  isCheckedA4: boolean;
  isCheckedB1: boolean;
  isCheckedB2: boolean;
  isCheckedB3: boolean;
  isCheckedB4: boolean;
}
export default function FormUpdateRole() {
  const [dataRole, setDataRole] = useState<Role>({
    nameRole: "",
    describe: "",
    isCheckedA1: false,
    isCheckedA2: false,
    isCheckedA3: false,
    isCheckedA4: false,
    isCheckedB1: false,
    isCheckedB2: false,
    isCheckedB3: false,
    isCheckedB4: false,
  });
  console.log("dataRole", dataRole.nameRole);
  const navigate = useNavigate();
  const [nameRole, setNameRole] = useState(dataRole.nameRole);
  const [describe, setDescribe] = useState(dataRole.describe);
  const [isCheckedA1, setIsCheckedA1] = useState(dataRole.isCheckedA1);
  const [isCheckedA2, setIsCheckedA2] = useState(dataRole.isCheckedA2);
  const [isCheckedA3, setIsCheckedA3] = useState(dataRole.isCheckedA3);
  const [isCheckedA4, setIsCheckedA4] = useState(dataRole.isCheckedA4);
  const [isCheckedB1, setIsCheckedB1] = useState(dataRole.isCheckedA1);
  const [isCheckedB2, setIsCheckedB2] = useState(dataRole.isCheckedA2);
  const [isCheckedB3, setIsCheckedB3] = useState(dataRole.isCheckedA3);
  const [isCheckedB4, setIsCheckedB4] = useState(dataRole.isCheckedA4);
  const formUpdateRole = {
    nameRole,
    describe,
    isCheckedA1,
    isCheckedA2,
    isCheckedA3,
    isCheckedA4,
    isCheckedB1,
    isCheckedB2,
    isCheckedB3,
    isCheckedB4,
  };
  console.log("formUpdateRole", formUpdateRole);
  const param = useParams().id;
  firebase.initializeApp(firebaseConfig);
  const database = firebase.database();
  useEffect(() => {
    setNameRole(dataRole.nameRole);
    setDescribe(dataRole.describe);
    setIsCheckedA1(dataRole.isCheckedA1);
    setIsCheckedA2(dataRole.isCheckedA2);
    setIsCheckedA3(dataRole.isCheckedA3);
    setIsCheckedA4(dataRole.isCheckedA4);
    setIsCheckedB1(dataRole.isCheckedB1);
    setIsCheckedB2(dataRole.isCheckedB2);
    setIsCheckedB3(dataRole.isCheckedB3);
    setIsCheckedB4(dataRole.isCheckedB4);
  }, [dataRole]);
  useEffect(() => {
    database
      .ref(`roles/${param}`)
      .once("value")
      .then((snapshot) => {
        const roleData = snapshot.val();
        setDataRole(roleData);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const handleUpdateRole = () => {
    database
      .ref(`roles/${param}`)
      .update(formUpdateRole)
      .then((res) => {
        console.log(res);
        message.success("Cập nhật vai trò thành công");
      })
      .catch((err) => {
        console.log(err);
        message.error("Cập nhật vai trò thất bại");
      });
  };
  return (
    <div className="mx-6 mb-4">
      <div className="bg-white rounded-2xl px-6 py-4">
        <p className="text-left text-orange-500 font-bold text-xl">
          Thông tin vai trò
        </p>
        <div className="grid grid-cols-2">
          <div>
            <span className="flex font-semibold mt-3 mb-2">
              <p>Tên vai trò</p>
              <p className="text-red-500 ml-1">*</p>
            </span>
            <input
              defaultValue={nameRole}
              onChange={(e) => setNameRole(e.target.value)}
              style={{ width: 560 }}
              className="w border-2 border-gray-100 rounded-lg py-1 pl-3 flex"
              type="text"
              placeholder="Nhập tên vai trò"
            />
            <span className="flex font-semibold mt-3 mb-2">
              <p>Mô tả:</p>
            </span>
            <textarea
              defaultValue={describe}
              onChange={(e) => setDescribe(e.target.value)}
              style={{ width: 560, height: 160, verticalAlign: "top" }}
              className="w border-2 border-gray-100 rounded-lg pl-3 flex"
              placeholder="Nhập mô tả"
            ></textarea>
            <span className="flex items-center mt-4 text-sm font-normal ">
              <p className="text-red-500 ml-1 mr-1">*</p>
              <p className="text-gray-400">Là trường thông tin bắt buộc</p>
            </span>
          </div>
          <div className="">
            <span className="flex font-semibold mt-3 mb-2">
              <p>Phân quyền chức năng</p>
              <p className="text-red-500 ml-1">*</p>
            </span>
            <div className="rounded-lg bg-orange-50 text-left pl-6 pt-4 pb-8">
              {/* //Nhóm chức năng AAAAAAAAAAAAAAAAAA */}
              <p className="text-xl text-orange-500 font-bold">
                Nhóm chức năng A
              </p>
              <div className="my-3 flex">
                <input
                  checked={isCheckedA1}
                  onChange={() => {
                    setIsCheckedA1(!isCheckedA1);
                  }}
                  className="mr-2"
                  type="checkbox"
                />

                <p>Tất cả</p>
              </div>
              <div className="my-3 flex">
                <input
                  checked={isCheckedA2}
                  onChange={() => {
                    setIsCheckedA2(!isCheckedA2);
                  }}
                  className="mr-2"
                  type="checkbox"
                />

                <p>Chức năng x</p>
              </div>
              <div className="my-3 flex">
                <input
                  checked={isCheckedA3}
                  onChange={() => {
                    setIsCheckedA3(!isCheckedA3);
                  }}
                  className="mr-2"
                  type="checkbox"
                />

                <p>Chức năng y</p>
              </div>
              <div className="my-3 flex">
                <input
                  checked={isCheckedA4}
                  onChange={() => {
                    setIsCheckedA4(!isCheckedA4);
                  }}
                  className="mr-2"
                  type="checkbox"
                />

                <p>Chức năng z</p>
              </div>
              {/* //Nhóm chức năng BBBBBBBBBBBBBBBBBB */}

              <p className="text-xl text-orange-500 font-bold">
                Nhóm chức năng B
              </p>
              <div className="my-3 flex">
                <input
                  checked={isCheckedB1}
                  onChange={() => {
                    setIsCheckedB1(!isCheckedB1);
                  }}
                  className="mr-2"
                  type="checkbox"
                />

                <p>Tất cả</p>
              </div>
              <div className="my-3 flex">
                <input
                  checked={isCheckedB2}
                  onChange={() => {
                    setIsCheckedB2(!isCheckedB2);
                  }}
                  className="mr-2"
                  type="checkbox"
                />

                <p>Chức năng x</p>
              </div>
              <div className="my-3 flex">
                <input
                  checked={isCheckedB3}
                  onChange={() => {
                    setIsCheckedB3(!isCheckedB3);
                  }}
                  className="mr-2"
                  type="checkbox"
                />

                <p>Chức năng y</p>
              </div>
              <div className="my-3 flex">
                <input
                  checked={isCheckedB4}
                  onChange={() => {
                    setIsCheckedB4(!isCheckedB4);
                  }}
                  className="mr-2"
                  type="checkbox"
                />

                <p>Chức năng z</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div style={{ bottom: -80 }} className="flex justify-center w-full mt-6">
        <NavLink to={"/rolemanager"}>
          <button className="bg-orange-50 border-2 border-orange-400 text-orange-400 px-14 py-3 rounded-lg mr-8">
            Hủy bỏ
          </button>
        </NavLink>
        <button
          onClick={() => {
            handleUpdateRole();
            navigate("/rolemanager");
          }}
          className="bg-orange-500 text-white px-14 py-3 rounded-lg"
        >
          Cập nhật
        </button>
      </div>
    </div>
  );
}
