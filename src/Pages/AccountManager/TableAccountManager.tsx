import React, { useEffect, useState } from "react";
import { Space, Table, Tag } from "antd";
import type { ColumnsType } from "antd/es/table";
import "../../mycss.css";
// firebase
import firebase from "firebase/compat/app";
import "firebase/compat/database";
import { firebaseConfig } from "../../firebase/firebase";
import { Url_Pb } from "./../../utils/Utils_Url";
import { NavLink, useNavigate } from "react-router-dom";
interface Role {
  email: string;
  fullName: string;
  password: string;
  phoneNumber: string;
  role: string;
  status: string;
  userName: string;
  action: any;
}
export default function TableAccountManager() {
  const [Userdata, setUserData] = useState<Role[]>([]);
  const navigate = useNavigate();
  console.log("Userdata", Userdata);
  firebase.initializeApp(firebaseConfig);
  // Truy cập vào cơ sở dữ liệu
  const db = firebase.database();
  const usersRef = db.ref("users");

  // Lấy danh sách users
  useEffect(() => {
    usersRef.once("value", (snapshot) => {
      const users = snapshot.val();
      // const arr = Object.values(users) as User[];

      setUserData(users);
    });
  }, []);

  const columns: ColumnsType<Role> = [
    {
      title: "Tên đăng nhập",
      dataIndex: "userName",
      key: "userName",
      render: (text) => <a className="">{text}</a>,
    },
    {
      title: "Họ tên",
      dataIndex: "fullName",
      key: "fullName",
    },
    {
      title: "Số điện thoại",
      dataIndex: "phoneNumber",
      key: "phoneNumber",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Vai trò",
      dataIndex: "status",
      key: "status",
    },
    {
      title: "Trạng thái hoạt động",
      dataIndex: "role",
      key: "role",
      render: (text: any) => {
        if (text == "hoatdong") {
          return (
            <div className="flex items-center">
              <div className="bg-green-500 w-2 h-2 rounded-full mr-1"></div>
              <p>Hoạt động</p>
            </div>
          );
        } else if (text == "ngunghoatdong") {
          return (
            <div className="flex items-center">
              <div className="bg-red-500 w-2 h-2 rounded-full mr-1"></div>
              <p>Ngưng hoạt động</p>
            </div>
          );
        }
      },
    },

    {
      title: " ",
      dataIndex: "action",
      key: "action",
    },
  ];
  const rederUserData = (): readonly Role[] => {
    const arr = Object.values(Userdata) as Role[];
    console.log("arr", arr);
    return arr.map((item, index) => {
      // const firstObj: any = Object.values<Record<string, any>>(Userdata)[index];
      //lấy key của obj để truyền vào cập nhật
      const ObjKey: string = Object.keys(Userdata)[index];
      console.log("firstObj", ObjKey);
      return {
        ...item,
        action: (
          <p
            onClick={() => {
              navigate(`/accountmanager/update/${ObjKey}`);
            }}
            className="text-blue-500 underline cursor-pointer"
          >
            Cập nhật
          </p>
        ),
      };
    });
  };
  return (
    <div className="ml-6 mt-11 pr-28">
      <p className="text-orange-500 text-2xl font-bold text-left mb-4">
        Danh sách tài khoản
      </p>
      <div className="flex justify-between mb-4">
        <div>
          <p className="text-left text-base font-semibold mb-1">Tên vai trò</p>
          <select
            style={{ width: 300 }}
            className="py-1 border-2 border-gray-100 rounded-lg pl-3"
          >
            <option value="" className="hover:bg-orange-400">
              Tất cả
            </option>
            <option value="ngunghoatdong" className="hover:bg-orange-400">
              Ngưng hoạt động
            </option>
            <option value="hoatdong" className="hover:bg-orange-400">
              Hoạt động
            </option>
          </select>
        </div>
        <div className="relative">
          <p className="text-left text-base font-semibold mb-1">Từ khoá</p>
          <input
            placeholder="Nhập từ khóa"
            className="py-1 border-2 border-gray-100 rounded-lg pl-3 w-72"
            type="text"
          />
          <img
            className="absolute right-3 top-9"
            src={Url_Pb + "/img/accountmanager/fi_search.png"}
            alt=""
          />
        </div>
      </div>
      <Table className="" columns={columns} dataSource={rederUserData()} />
      {/* rederUserData()  */}
      <NavLink to={"/accountmanager/add"}>
        <img
          style={{ top: 235 }}
          className="absolute  right-0 cursor-pointer"
          src={Url_Pb + "/img/accountmanager/Component 2.png"}
          alt=""
        />
      </NavLink>
    </div>
  );
}
