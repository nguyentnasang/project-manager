import React from "react";
import HeaderInfor from "../../component/HeaderInfor";
import FormAddService from "./FormAddService";

export default function AddService() {
  return (
    <div className="bg-gray-100 h-full">
      <header className="flex justify-between pt-7 ml-7">
        <div className="flex text-xl text-gray-500 font-bold">
          <p className=" cursor-pointer mr-4">Dịch vụ</p>
          <p>{">"}</p>
          <p className=" cursor-pointer mr-4 ml-4">Danh sách dịch vụ</p>
          <p>{">"}</p>
          <p className=" cursor-pointer text-orange-500 ml-4">Thêm dịch vụ</p>
        </div>
        <div>
          <HeaderInfor />
        </div>
      </header>
      <div className="text-left mt-9 ml-6 mb-3">
        <span className="text-orange-500 text-2xl font-bold ">
          Quản lý dịch vụ
        </span>
        <div className="mr-6">
          <FormAddService />
        </div>
      </div>
    </div>
  );
}
