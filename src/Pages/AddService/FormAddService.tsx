import React from "react";
import { Button, Checkbox, Form, Input, Row, message } from "antd";
import TextArea from "antd/es/input/TextArea";
import { NavLink, useNavigate } from "react-router-dom";
import firebase from "firebase/compat/app";
import { firebaseConfig } from "../../firebase/firebase";
import { useSelector } from "react-redux";
export default function FormAddService() {
  const userLogin = useSelector((state: any) => state.userSlice.userLogin);

  const navigate = useNavigate();
  firebase.initializeApp(firebaseConfig);
  const db = firebase.database();
  const onFinish = async (values: any) => {
    console.log("Success:", values);
    // kiểm tra mã dịch vụ có tồn tại trong hệ thống
    const codeServiceRef = db
      .ref("services")
      .orderByChild("codeService")
      .equalTo(values.codeService);
    const codeServiceSnapshot = await codeServiceRef.once("value");
    if (codeServiceSnapshot.exists()) {
      message.error("Mã dịch vụ đã có trong hệ thống");
      return;
    }
    //kiểm tra tên dịch vụ có tồn tại trong hệ thống
    const nameServiceRef = db
      .ref("services")
      .orderByChild("nameService")
      .equalTo(values.nameService);
    const nameServiceSnapshot = await nameServiceRef.once("value");
    if (nameServiceSnapshot.exists()) {
      message.error("tên dịch vụ đã tồn tại");
      return;
    }
    //kiểm tra xong nếu kh có vấn đề thì thêm vào
    db.ref("services").push(values);
    message.success("thêm dịch vụ thành công");
    db.ref("activities").push({
      userName: userLogin.fullName,
      dateTime: `${new Date()}`,
      IP: values.codeService,
      operation: "thêm dịch vụ",
    });
    navigate("/servicemanager");
  };
  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="mt-8">
      <Form
        layout="vertical"
        name="basic"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <div className="bg-white pt-4 pl-6 pb-3 rounded-2xl">
          <p className="text-orange-500 text-xl font-bold mb-3">
            Thông tin dịch vụ
          </p>
          <div className="grid grid-cols-2 gap-6">
            <div>
              <Form.Item
                label={
                  <div className="flex">
                    <p className="mr-1 font-semibold">Mã dịch vụ: </p>
                    <p className="text-red-500 font-bold">*</p>
                  </div>
                }
                name="codeService"
                rules={[
                  { required: true, message: "Vui lòng nhập mã dịch vụ!" },
                ]}
              >
                <Input placeholder="Nhập mã dịch vụ" />
              </Form.Item>

              <Form.Item
                label={
                  <div className="flex">
                    <p className="mr-1 font-semibold">Tên dịch vụ : </p>
                    <p className="text-red-500 font-bold">*</p>
                  </div>
                }
                name="nameService"
                rules={[
                  { required: true, message: "Vui lòng nhập tên dịch vụ!" },
                ]}
              >
                <Input placeholder="Nhập tên dịch vụ" />
              </Form.Item>
            </div>
            <Form.Item
              label={<p className="mr-1 font-semibold">Mô tả:</p>}
              name="descService"
              rules={[{ required: true, message: "Vui lòng nhập mô tả!" }]}
            >
              <TextArea rows={5} placeholder="Nhập mô tả" />
            </Form.Item>
          </div>
          <div>
            <p className="text-orange-500 text-xl font-bold mb-3">
              Quy tắc cấp số
            </p>

            <Form.Item name="numberRule">
              <Checkbox.Group className="grid grid-cols-8">
                <div className="flex flex-col col-span-1">
                  <Checkbox className="font-semibold mt-1" value="auto">
                    Tăng tự động từ:
                  </Checkbox>

                  <Checkbox
                    className="font-semibold py-6"
                    value="prefix"
                    style={{ lineHeight: "32px" }}
                  >
                    Prefix:
                  </Checkbox>

                  <Checkbox
                    className="font-semibold pb-6"
                    value="surfix"
                    style={{ lineHeight: "32px" }}
                  >
                    Surfix:
                  </Checkbox>

                  <Checkbox
                    className="font-semibold"
                    value="reset"
                    style={{ lineHeight: "32px" }}
                  >
                    Reset mỗi ngày
                  </Checkbox>
                </div>

                <div className="">
                  <div className="flex items-center">
                    <p className="rounded-lg border-2 border-gray-200 py-1 px-3">
                      0001
                    </p>
                    <span className="mx-3">đến</span>
                    <p className="rounded-lg border-2 border-gray-200 py-1 px-3">
                      9999
                    </p>
                  </div>
                  <div className="flex my-5">
                    <p className="rounded-lg border-2 border-gray-200 py-1 px-3">
                      0001
                    </p>
                  </div>
                  <div className="flex">
                    <p className="rounded-lg border-2 border-gray-200 py-1 px-3">
                      0001
                    </p>
                  </div>
                </div>
              </Checkbox.Group>
            </Form.Item>
            <span className="flex items-center mt-4 text-sm font-normal ">
              <p className="text-red-500 ml-1 mr-1">*</p>
              <p className="text-gray-400">Là trường thông tin bắt buộc</p>
            </span>
          </div>
        </div>
        <Form.Item>
          <div className="flex text-center justify-center mt-6">
            <NavLink to={"/servicemanager"}>
              <button className="bg-orange-50 border-2 border-orange-400 text-orange-400 px-14 py-3 rounded-lg mr-8">
                Hủy bỏ
              </button>
            </NavLink>
            <Button
              className="bg-orange-500 text-white px-10 py-6 rounded-lg flex items-center"
              htmlType="submit"
            >
              Thêm dịch vụ
            </Button>
          </div>
        </Form.Item>
      </Form>
    </div>
  );
}
