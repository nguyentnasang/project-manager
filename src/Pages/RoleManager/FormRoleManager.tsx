import React, { useEffect, useState } from "react";
import { Url_Pb } from "../../utils/Utils_Url";
import { Table } from "antd";
import { ColumnsType } from "antd/es/table";
import { NavLink, useNavigate } from "react-router-dom";
import firebase from "firebase/compat/app";
import { firebaseConfig } from "../../firebase/firebase";
import { promises } from "dns";
interface User {
  nameRole: string;
  describe: string;
  total: string;
  action: any;
}
interface Role {
  nameRole: any;
}
export default function FormRoleManager() {
  const navigate = useNavigate();
  const [dataRoles, setDataRoles] = useState<User[]>([]);
  console.log("dataRoles", dataRoles);
  const [renderRoles, setRenderRoles] = useState<any>([]);
  // console.log("renderRoles", renderRoles);
  const columns: ColumnsType<User> = [
    {
      title: "Tên vai trò",
      dataIndex: "nameRole",
      key: "nameRole",
      render: (text) => <a className="">{text}</a>,
    },
    {
      title: "Số người dùng",
      dataIndex: "total",
      key: "total",
    },
    {
      title: "Mô tả",
      dataIndex: "describe",
      key: "describe",
    },
    {
      title: " ",
      dataIndex: "action",
      key: "action",
    },
  ];
  firebase.initializeApp(firebaseConfig);
  useEffect(() => {
    const database = firebase.database();
    const getRoles = () => {
      const roleRef = database.ref("roles");

      roleRef
        .once("value")
        .then((snapshot) => {
          const roles = snapshot.val();
          setDataRoles(roles);
          //   console.log("lấy thông tin thành công");
        })
        .catch((err) => {
          console.log("lấy thông tin thất bại");
        });
    };
    getRoles();
  }, []);

  const renderDataRoles = (): Promise<readonly User[]> => {
    const arr = Object.values(dataRoles) as User[];
    const users: User[] = [];
    const promises = arr.map((item, index) => {
      const ObjKey: string = Object.keys(dataRoles)[index];
      console.log("ObjKey", ObjKey);
      return firebase
        .database()
        .ref("users")
        .orderByChild("status")
        .equalTo(item.nameRole)
        .once("value")
        .then((snapshot) => {
          const numUsers = snapshot.numChildren();
          const user = {
            ...item,
            total: numUsers.toString(),
            action: (
              <p
                onClick={() => {
                  navigate(`/rolemanager/update/${ObjKey}`);
                }}
                className="text-blue-500 underline cursor-pointer"
              >
                Cập nhật
              </p>
            ),
          };
          users.push(user);
          console.log("users", users);
        });
    });
    return Promise.all(promises).then(() => users);
  };
  useEffect(() => {
    renderDataRoles() //khi component render lần đầu tiên, dữ liệu sẽ không được hiển thị vì hàm renderDataRoles() sẽ trả về một mảng rỗng và chưa có dữ liệu được lấy từ Firebase. đảm bảo rằng các giá trị state đã được cập nhật trước khi lấy dữ liệu. nên ta bỏ vào useEffect
      .then((data) => {
        console.log("data", data);
        setRenderRoles(data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [dataRoles]);

  return (
    <div className="ml-6 mt-11 pr-28 ">
      <div className="flex justify-between">
        <p className="text-orange-500 text-2xl font-bold text-left mb-4">
          Danh sách vai trò
        </p>
        <div className=" mb-4">
          <div className="relative">
            <p className="text-left text-base font-semibold mb-1">Từ khoá</p>
            <input
              placeholder="Nhập từ khóa"
              className="py-1 border-2 border-gray-100 rounded-lg pl-3 w-72"
              type="text"
            />
            <img
              className="absolute right-3 top-9"
              src={Url_Pb + "/img/accountmanager/fi_search.png"}
              alt=""
            />
          </div>
        </div>
      </div>
      <Table className="" columns={columns} dataSource={renderRoles} />
      {/* rederUserData().  */}
      <NavLink to={"/rolemanager/add"}>
        <img
          style={{ top: 235 }}
          className="absolute  right-0 cursor-pointer"
          src={Url_Pb + "/img/rolemanager/Component 2.png"}
          alt=""
        />
      </NavLink>
    </div>
  );
}
