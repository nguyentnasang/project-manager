import React from "react";
import HeaderInfor from "../../component/HeaderInfor";
import FormRoleManager from "./FormRoleManager";

export default function RoleManager() {
  return (
    <div>
      <header className="flex justify-between pt-7 ml-7">
        <div className="flex text-xl text-gray-500 font-bold">
          <p className=" cursor-pointer mr-4">Cài đặt hệ thống</p>
          <p>{">"}</p>

          <p className=" cursor-pointer text-orange-500 ml-4">
            Quản lý tài khoản
          </p>
        </div>
        <div>
          <HeaderInfor />
        </div>
      </header>
      <FormRoleManager />
    </div>
  );
}
