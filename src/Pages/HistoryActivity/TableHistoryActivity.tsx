import { DatePicker, DatePickerProps, Space, Table } from "antd";
import React, { useEffect, useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import { Url_Pb } from "../../utils/Utils_Url";
import { ColumnsType } from "antd/es/table";
import firebase from "firebase/compat/app";
import { firebaseConfig } from "../../firebase/firebase";
import { log } from "console";
interface Service {
  codeService: string;
  nameService: string;
  descService: string;
  status: string;
  detail: any;
  action: any;
}
export default function TableHistoryActivity() {
  const [dataActivity, setDataActivity] = useState<Service[]>([]);
  const onChange: DatePickerProps["onChange"] = (date, dateString) => {
    console.log(date, dateString);
  };
  const columns: ColumnsType<Service> = [
    {
      title: "Tên dịch vụ",
      dataIndex: "userName",
      key: "userName",
      render: (text: any) => <a className="">{text}</a>,
    },
    {
      title: "Thời gian tác động",
      dataIndex: "dateTime",
      key: "dateTime",
    },
    {
      title: "IP thực hiện",
      dataIndex: "IP",
      key: "IP",
      ellipsis: {
        showTitle: false,
      },
    },
    {
      title: "Thao tác thực hiện",
      dataIndex: "operation",
      key: "operation",
    },
  ];
  firebase.initializeApp(firebaseConfig);
  const db = firebase.database();
  useEffect(() => {
    db.ref("activities")
      .once("value")
      .then((res) => {
        setDataActivity(res.val());
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const renderData = () => {
    const arr = Object.values(dataActivity);
    console.log("arr", arr);
    return arr.map((item: any) => {
      const dataTime = new Date(item.dateTime).toLocaleString("vi-VN");

      return { ...item, dateTime: `${dataTime}` };
    });
  };
  renderData();
  return (
    <div>
      <div className="ml-6 mt-11 pr-28">
        <p className="text-orange-500 text-2xl font-bold text-left mb-4">
          Quản lý dịch vụ
        </p>
        <div className="flex justify-between mb-4">
          <div className="flex gap-6">
            <div>
              <p className="text-left text-base font-semibold mb-1">
                Chọn thời gian
              </p>
              <Space direction="vertical">
                <div className="flex items-center gap-2">
                  <DatePicker onChange={onChange} />
                  <p className="font-bold text-sm">{">"}</p>
                  <DatePicker onChange={onChange} />
                </div>
              </Space>
            </div>
          </div>
          <div className="relative">
            <p className="text-left text-base font-semibold mb-1">Từ khoá</p>
            <input
              placeholder="Nhập từ khóa"
              className="py-1 border-2 border-gray-100 rounded-lg pl-3 w-72"
              type="text"
            />
            <img
              className="absolute right-3 top-9"
              src={Url_Pb + "/img/accountmanager/fi_search.png"}
              alt=""
            />
          </div>
        </div>
        <Table className="" columns={columns} dataSource={renderData()} />
      </div>
    </div>
  );
}
