import React, { useEffect, useState } from "react";
import { Space, Table, Tag } from "antd";
import type { ColumnsType } from "antd/es/table";
import firebase from "firebase/compat/app";
import { firebaseConfig } from "../../firebase/firebase";
import { useParams } from "react-router";
interface DataType {
  key: string;
  name: string;
  age: number;
  address: string;
  tags: string[];
}
const columns: ColumnsType<DataType> = [
  {
    title: "Số thứ tự",
    dataIndex: "serial",
    key: "serial",
    render: (text) => <a>{text}</a>,
  },
  {
    title: "Trạng thái",
    dataIndex: "status",
    key: "status",
  },
];

firebase.initializeApp(firebaseConfig);
const db = firebase.database();
export default function TableDetailService() {
  const param = useParams().id;
  const [dataService, setDataService] = useState<any>();
  const [dataRender, setDataRender] = useState<any>();
  console.log("dataRender", dataRender);
  console.log("dataService", dataService);

  useEffect(() => {
    db.ref(`services/${param}`)
      .once("value")
      .then((res) => {
        setDataService(res.val());

        // Xử lý dữ liệu trả về
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  useEffect(() => {
    if (dataService) {
      console.log("sang", dataService.nameService);
      db.ref(`serials`)
        .orderByChild("nameService")
        .equalTo(dataService.nameService)
        .once("value", (snapshot) => {
          // Xử lý dữ liệu trả về
          setDataRender(snapshot.val());
        });
    }
  }, [dataService]);
  const renderDate = () => {
    const arr = dataRender ? Object.values(dataRender) : [];
    return arr.map((item: any) => {
      return { ...item };
    });
  };
  return <Table columns={columns} dataSource={renderDate()} />;
}
