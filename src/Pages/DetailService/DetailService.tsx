import React, { useEffect, useState } from "react";
import HeaderInfor from "../../component/HeaderInfor";
import { useParams } from "react-router";
import firebase from "firebase/compat/app";
import { firebaseConfig } from "../../firebase/firebase";
import { DatePicker, DatePickerProps, Space } from "antd";
import { Url_Pb } from "../../utils/Utils_Url";
import TableDetailService from "./TableDetailService";

export default function DetailService() {
  const param = useParams().id;
  const [dataService, setDataService] = useState<any>([]);
  const onChange: DatePickerProps["onChange"] = (date, dateString) => {
    console.log(date, dateString);
  };
  console.log("dataService", dataService);
  firebase.initializeApp(firebaseConfig);
  const db = firebase.database();
  useEffect(() => {
    db.ref(`services/${param}`)
      .once("value")
      .then((res) => {
        setDataService(res.val());
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const renderRules = () => {
    return dataService.numberRule?.map((item: any) => {
      console.log("item", item);
      if (item === "auto") {
        return (
          <div className="flex col-span-1 mt-3">
            <div className="font-semibold mt-1">Tăng tự động:</div>
            <div className="flex items-center">
              <p className="rounded-lg border-2 border-gray-200 py-1 px-3">
                0001
              </p>
              <span className="mx-3">đến</span>
              <p className="rounded-lg border-2 border-gray-200 py-1 px-3">
                9999
              </p>
            </div>
          </div>
        );
      } else if (item === "prefix") {
        return (
          <div className="flex col-span-1 mt-3">
            <div className="font-semibold mt-1">Prefix:</div>
            <div className="flex items-center">
              <div className="">
                <p className="rounded-lg border-2 border-gray-200 py-1 px-3">
                  0001
                </p>
              </div>
            </div>
          </div>
        );
      } else if (item === "surfix") {
        return (
          <div className="flex col-span-1 mt-3">
            <div className="font-semibold mt-1">Surfix:</div>
            <div className="flex items-center">
              <div className="">
                <p className="rounded-lg border-2 border-gray-200 py-1 px-3">
                  0001
                </p>
              </div>
            </div>
          </div>
        );
      } else if (item === "reset") {
        return (
          <div className="flex col-span-1 mt-3">
            <div className="font-semibold mt-1">Reset mỗi ngày</div>
          </div>
        );
      }
    });
  };
  return (
    <div className="bg-gray-100 w-full pb-8 h-full">
      <header className="flex justify-between pt-7 ml-7">
        <div className="flex text-xl text-gray-500 font-bold">
          <p className=" cursor-pointer mr-4">Cài đặt hệ thống</p>
          <p>{">"}</p>
          <p className=" cursor-pointer mr-4 ml-4">Quản lý vai trò</p>
          <p>{">"}</p>
          <p className=" cursor-pointer text-orange-500 ml-4">
            Chi tiết thiết bị
          </p>
        </div>
        <div>
          <HeaderInfor />
        </div>
      </header>
      <div className="ml-6 mt-11 mr-24">
        <p className="text-2xl text-orange-500 font-bold text-left">
          Quản lý dịch vụ
        </p>
        <div className="grid grid-cols-3 gap-6">
          <div className="mt-8 bg-white col-span-1 rounded-xl pt-4 pl-4">
            <p className="text-xl text-orange-500 font-bold text-left mb-3">
              Thông tin dịch vụ
            </p>
            <div className="flex">
              <div className="text-left">
                <p className="text-base font-semibold">Mã dịch vụ: </p>
                <p className="text-base font-semibold my-3">Tên dịch vụ: </p>
                <p className="text-base font-semibold">Mô tả:</p>
              </div>
              <div className="text-left ml-5 ">
                <p>{dataService.codeService}</p>
                <p className=" my-3">{dataService.nameService}</p>
                <p>
                  {dataService.descService?.length > 30
                    ? dataService.descService.slice(0, 30) + "..."
                    : dataService.descService}
                </p>
              </div>
            </div>
            <div>
              <p className="text-xl text-orange-500 font-bold text-left mt-3">
                Quy tắc cấp số
              </p>
              {renderRules()}
            </div>
          </div>
          <div className="col-span-2 bg-white mt-8 rounded-xl px-6 pt-4">
            <div className="flex justify-between mb-4">
              <div className="flex gap-5">
                <div>
                  <p className="text-left text-base font-semibold mb-1">
                    Trạng thái hoạt động
                  </p>
                  <select
                    style={{ width: 160 }}
                    className="py-1 border-2 border-gray-100 rounded-lg pl-3"
                  >
                    <option value="" className="hover:bg-orange-400">
                      Tất cả
                    </option>
                    <option
                      value="ngunghoatdong"
                      className="hover:bg-orange-400"
                    >
                      Ngưng hoạt động
                    </option>
                    <option value="hoatdong" className="hover:bg-orange-400">
                      Hoạt động
                    </option>
                  </select>
                </div>
                <div className="mr-5">
                  <p className="text-left text-base font-semibold mb-1">
                    Chọn thời gian
                  </p>
                  <Space direction="vertical">
                    <div className="flex items-center gap-2">
                      <DatePicker onChange={onChange} />
                      <p className="font-bold text-sm">{">"}</p>
                      <DatePicker onChange={onChange} />
                    </div>
                  </Space>
                </div>
              </div>
              <div className="relative">
                <p className="text-left text-base font-semibold mb-1">
                  Từ khoá
                </p>
                <input
                  placeholder="Nhập từ khóa"
                  className="py-1 border-2 border-gray-100 rounded-lg pl-3 w-72"
                  type="text"
                />
                <img
                  className="absolute right-3 top-9"
                  src={Url_Pb + "/img/accountmanager/fi_search.png"}
                  alt=""
                />
              </div>
            </div>
            <TableDetailService />
          </div>
        </div>
      </div>
    </div>
  );
}
