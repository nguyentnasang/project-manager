import React from "react";
import ChartDashboard from "./ChartDashboard";
import Calendar from "./Calendar";
import NumericalOrder from "./NumericalOrder";
import HeaderInfor from "../../component/HeaderInfor";
import Overview from "./Overview";

export default function Dashboard() {
  return (
    <div className="bg-gray-100 h-full grid grid-cols-3">
      <div className="col-span-2 px-6 py-8">
        <NumericalOrder />
        <ChartDashboard />
      </div>
      <div className="col-span-1 bg-white px-6">
        <div className="ml-20 mt-6 mb-10">
          <HeaderInfor />
        </div>
        <Overview />
        <div>
          <Calendar />
        </div>
      </div>
    </div>
  );
}
