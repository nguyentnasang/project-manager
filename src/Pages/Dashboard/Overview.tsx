import React, { useEffect, useState } from "react";
import { Url_Pb } from "../../utils/Utils_Url";
import firebase from "firebase/compat/app";
import { firebaseConfig } from "../../firebase/firebase";

export default function Overview() {
  const [seriCount, setSeriCount] = useState(0);
  const [servicesCount, setServicesCount] = useState(0);
  const [devicesCount, setDevicesCount] = useState(0);
  console.log("seriCount", seriCount);
  firebase.initializeApp(firebaseConfig);
  const db = firebase.database();
  useEffect(() => {
    const servicesRef = db.ref("serials");

    servicesRef.once("value", (snapshot) => {
      setSeriCount(snapshot.numChildren());
    });
  }, []);
  useEffect(() => {
    const servicesRef = db.ref("services");

    servicesRef.once("value", (snapshot) => {
      setServicesCount(snapshot.numChildren());
    });
  }, []);
  useEffect(() => {
    const servicesRef = db.ref("devices");

    servicesRef.once("value", (snapshot) => {
      setDevicesCount(snapshot.numChildren());
    });
  }, []);
  return (
    <div>
      <p className="text-2xl font-bold text-orange-500 text-left mb-4">
        Tổng quan
      </p>
      {/* thiết bị  */}
      <div
        style={{ boxShadow: "2px 2px 15px rgba(70, 64, 67, 0.1)" }}
        className="flex items-center   py-3 pl-4 rounded-xl"
      >
        <img src={Url_Pb + "./img/dashboard/calender/capso.png"} alt="" />
        <div className=" ml-3 ">
          <p className="text-2xl font-extrabold">{devicesCount}</p>
          <div className="flex items-center">
            <img
              className="h-4"
              src={Url_Pb + "./img/dashboard/calender/icon.png"}
              alt=""
            />
            <p className="text-orange-500 font-semibold ml-1">Thiết bị</p>
          </div>
        </div>
        <div className="grid grid-cols-5 text-left items-center ml-10">
          <div className="text-sm text-gray-400 col-span-4">
            <div className="flex items-center">
              <div className="w-1 h-1 bg-yellow-500 rounded-full mr-1"></div>
              <p>Đang hoạt động</p>
            </div>
            <div className="flex items-center mt-1">
              <div className="w-1 h-1 bg-gray-400 rounded-full mr-1"></div>
              <p>Ngưng hoạt động</p>
            </div>
          </div>
          <div className="text-sm text-orange-500 font-bold ml-1 col-span-1">
            <p>...</p>
            <p className="mt-1">...</p>
          </div>
        </div>
      </div>
      {/* dịch vụ */}
      <div
        style={{ boxShadow: "2px 2px 15px rgba(70, 64, 67, 0.1)" }}
        className="flex items-center  py-3 pl-4 rounded-xl my-3"
      >
        <img src={Url_Pb + "./img/dashboard/calender/dichvu.png"} alt="" />
        <div className=" ml-3 ">
          <p className="text-2xl font-extrabold">{servicesCount}</p>
          <div className="flex items-center">
            <img
              className="h-4"
              src={Url_Pb + "./img/dashboard/calender/icondichvu.png"}
              alt=""
            />
            <p className="text-blue-500 font-semibold ml-1">Dịch vụ</p>
          </div>
        </div>
        <div className="grid grid-cols-5 text-left items-center ml-10">
          <div className="text-sm text-gray-400 col-span-4">
            <div className="flex items-center ">
              <div className="w-1 h-1 bg-blue-500 rounded-full mr-1"></div>
              <p>Đang hoạt động</p>
            </div>
            <div className="flex items-center mt-1">
              <div className="w-1 h-1 bg-gray-400 rounded-full mr-1"></div>
              <p>Ngưng hoạt động</p>
            </div>
          </div>
          <div className="text-sm text-blue-500 font-bold ml-1 col-span-1">
            <p>...</p>
            <p className="mt-1">...</p>
          </div>
        </div>
      </div>
      {/* cấp số */}
      <div
        style={{ boxShadow: "2px 2px 15px rgba(70, 64, 67, 0.1)" }}
        className="flex items-center  py-3 pl-4 rounded-xl mb-3"
      >
        <img src={Url_Pb + "./img/dashboard/calender/thietbi.png"} alt="" />
        <div className=" ml-3 ">
          <p className="text-2xl font-extrabold">{seriCount}</p>
          <div className="flex items-center">
            <img
              className="h-4"
              src={Url_Pb + "./img/dashboard/calender/iconcapso.png"}
              alt=""
            />
            <p className="text-green-500 font-semibold ml-1">Cấp số</p>
          </div>
        </div>
        <div className="grid grid-cols-5 text-left items-center ml-12">
          <div className="text-sm text-gray-400 col-span-4">
            <div className="flex items-center">
              <div className="w-1 h-1 bg-blue-500 rounded-full mr-1"></div>
              <p>Đã sử dụng</p>
            </div>
            <div className="flex items-center mt-1">
              <div className="w-1 h-1 bg-gray-400 rounded-full mr-1"></div>
              <p>Đang chờ</p>
            </div>
            <div className="flex items-center mt-1">
              <div className="w-1 h-1 bg-fuchsia-400 rounded-full mr-1"></div>
              <p>Bỏ qua</p>
            </div>
          </div>
          <div className="text-sm text-green-500 font-bold ml-1 col-span-1">
            <p>...</p>
            <p className="mt-1">...</p>
            <p className="mt-1">...</p>
          </div>
        </div>
      </div>
    </div>
  );
}
