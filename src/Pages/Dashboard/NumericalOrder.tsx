import React, { useEffect, useState } from "react";
import { Url_Pb } from "../../utils/Utils_Url";
import firebase from "firebase/compat/app";
import { firebaseConfig } from "../../firebase/firebase";

export default function NumericalOrder() {
  const [seriCount, setSeriCount] = useState(0);
  console.log("seriCount", seriCount);
  firebase.initializeApp(firebaseConfig);
  const db = firebase.database();
  useEffect(() => {
    const servicesRef = db.ref("serials");

    servicesRef.once("value", (snapshot) => {
      setSeriCount(snapshot.numChildren());
    });
  }, []);
  return (
    <div>
      <p className="text-left text-xl text-orange-500 font-bold mb-11">
        Dashboard
      </p>
      <p className="text-left text-2xl text-orange-500 font-bold mb-5">
        Biểu đồ cấp số
      </p>
      <div className="flex justify-between mb-4">
        <div className="bg-white rounded-xl w-44 pt-2 pl-3 pr-4 pb-2">
          <div className="flex items-center ">
            <img
              className="mr-3"
              src={Url_Pb + "./img/dashboard/calender.png"}
              alt=""
            />
            <p className="text-sm text-left font-bold text-gray-500">
              Số thứ tự <br /> đã cấp
            </p>
          </div>
          <div>
            <p className="text-left text-3xl font-bold mt-3 text-gray-500">
              {seriCount}
            </p>
          </div>
        </div>
        <div className="bg-white rounded-xl w-44 pt-2 pl-3 pr-4 pb-2">
          <div className="flex items-center ">
            <img
              className="mr-3"
              src={Url_Pb + "./img/dashboard/calendercheck.png"}
              alt=""
            />
            <p className="text-sm text-left font-bold text-gray-500">
              Số thứ tự <br /> đã sử dụng
            </p>
          </div>
          <div>
            <p className="text-left text-3xl font-bold mt-3 text-gray-500">0</p>
          </div>
        </div>
        <div className="bg-white rounded-xl w-44 pt-2 pl-3 pr-4 pb-2">
          <div className="flex items-center ">
            <img
              className="mr-3"
              src={Url_Pb + "./img/dashboard/phone.png"}
              alt=""
            />
            <p className="text-sm text-left font-bold text-gray-500">
              Số thứ tự <br /> đang chờ
            </p>
          </div>
          <div>
            <p className="text-left text-3xl font-bold mt-3 text-gray-500">
              {" "}
              {seriCount}
            </p>
          </div>
        </div>
        <div className="bg-white rounded-xl w-44 pt-2 pl-3 pr-4 pb-2">
          <div className="flex items-center ">
            <img
              className="mr-3"
              src={Url_Pb + "./img/dashboard/start.png"}
              alt=""
            />
            <p className="text-sm text-left font-bold text-gray-500">
              Số thứ tự <br />
              đã bỏ qua
            </p>
          </div>
          <div>
            <p className="text-left text-3xl font-bold mt-3 text-gray-500">0</p>
          </div>
        </div>
      </div>
    </div>
  );
}
