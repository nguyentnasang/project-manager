import React from "react";
import {
  AreaChart,
  Area,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  ResponsiveContainer,
} from "recharts";

const data = [
  {
    name: "Jan",
    uv: 4000,
    pv: 2400,
    amt: 2400,
  },
  {
    name: "Feb",
    uv: 3000,
    pv: 1398,
    amt: 2210,
  },
  {
    name: "Mar",
    uv: 2000,
    pv: 9800,
    amt: 2290,
  },
  {
    name: "Apr",
    uv: 2780,
    pv: 3908,
    amt: 2000,
  },
  {
    name: "May",
    uv: 1890,
    pv: 4800,
    amt: 2181,
  },
  {
    name: "Jun",
    uv: 2390,
    pv: 3800,
    amt: 2500,
  },
];
export default function ChartDashboard() {
  return (
    <div className="bg-white rounded-xl pl-6 py-6">
      <p className="text-left text-xl font-bold text-gray-500">
        Bảng thống kê theo tháng
      </p>
      <p className="text-left text-sm text-gray-400 mt-4 mb-3">Năm 2023</p>
      <ResponsiveContainer width="100%" height={400}>
        <AreaChart
          data={data}
          margin={{ top: 10, right: 30, left: 0, bottom: 0 }}
        >
          <CartesianGrid strokeDasharray="3 3" vertical={false} />
          <XAxis dataKey="name" />
          <YAxis />
          <Tooltip />
          <Area type="monotone" dataKey="pv" stroke="#5185F7" fill="#CEDDFF" />
          {/* <Area type="monotone" dataKey="uv" stroke="#82ca9d" fill="#82ca9d" /> */}
        </AreaChart>
      </ResponsiveContainer>
    </div>
  );
}
