import { DatePicker, DatePickerProps, Space, Table } from "antd";
import React, { useEffect, useState } from "react";
import { Url_Pb } from "../../utils/Utils_Url";
import { NavLink } from "react-router-dom";
import { ColumnsType, TableProps } from "antd/es/table";
import firebase from "firebase/compat/app";
import { firebaseConfig } from "../../firebase/firebase";
import { FilterValue, SorterResult } from "antd/es/table/interface";
interface Serial {
  serial: number;
  nameService: string;
  dataTime: string;
  expiry: string;
  status: string;
  origin: string;
  action: any;
}
interface DataType {
  key: string;
  nameService: string;
  serial: number;
  status: string;
  origin: string;
  dateTime: number;
}

export default function TableReportManager() {
  // .....................
  const [filteredInfo, setFilteredInfo] = useState<
    Record<string, FilterValue | null>
  >({});
  const [sortedInfo, setSortedInfo] = useState<SorterResult<DataType>>({});

  const handleChange: TableProps<DataType>["onChange"] = (
    pagination,
    filters,
    sorter
  ) => {
    console.log("Various parameters", pagination, filters, sorter);
    setFilteredInfo(filters);
    setSortedInfo(sorter as SorterResult<DataType>);
  };

  const clearFilters = () => {
    setFilteredInfo({});
  };

  const clearAll = () => {
    setFilteredInfo({});
    setSortedInfo({});
  };

  const setAgeSort = () => {
    setSortedInfo({
      order: "descend",
      columnKey: "age",
    });
  };
  // ....................
  const [dataSeri, setDataSeri] = useState<Serial[]>([]);
  console.log("dataSeri", dataSeri);

  const columns: ColumnsType<DataType> = [
    {
      title: "STT",
      dataIndex: "serial",
      key: "serial",
      sorter: (a, b) => a.serial - b.serial,
      sortOrder: sortedInfo.columnKey === "serial" ? sortedInfo.order : null,
      ellipsis: true,
    },

    {
      title: "Tên dịch vụ",
      dataIndex: "nameService",
      key: "nameService",

      sorter: (a, b) => a.nameService.length - b.nameService.length,
      sortOrder:
        sortedInfo.columnKey === "nameService" ? sortedInfo.order : null,
      ellipsis: true,
    },
    {
      title: "Thời gian cấp",
      dataIndex: "dateTime",
      key: "dateTime",
      sorter: (a, b) => a.dateTime - b.dateTime,
      sortOrder: sortedInfo.columnKey === "dateTime" ? sortedInfo.order : null,
      ellipsis: true,
    },

    {
      title: "Tình trạng",
      dataIndex: "status",
      key: "status",
      render: (text: any) => {
        if (text == "Đang chờ") {
          return (
            <div className="flex items-center">
              <div className="bg-blue-500 w-2 h-2 rounded-full mr-1"></div>
              <p>Đang chờ </p>
            </div>
          );
        }
      },
      sorter: (a, b) => a.status.length - b.status.length,
      sortOrder: sortedInfo.columnKey === "status" ? sortedInfo.order : null,
      ellipsis: true,
    },
    {
      title: "Nguồn cấp",
      dataIndex: "origin",
      key: "origin",
      sorter: (a, b) => a.origin.length - b.origin.length,
      sortOrder: sortedInfo.columnKey === "origin" ? sortedInfo.order : null,
      ellipsis: true,
    },
  ];
  firebase.initializeApp(firebaseConfig);
  const db = firebase.database();
  useEffect(() => {
    db.ref("serials").once("value", (snapshot) => {
      setDataSeri(snapshot.val());
    });
  }, []);
  const renderSeri = () => {
    const arrService = Object.values(dataSeri);
    return arrService.map((item: any, index) => {
      const dateTime = new Date(item.dateTime).toLocaleString("vi-VN");
      const expiry = new Date(item.expiry).toLocaleString("vi-VN");
      //   const ObjKey = Object.keys(dataSeri)[index];
      return {
        ...item,
        dateTime: `${dateTime}`,
        expiry: `${expiry}`,
      };
    });
  };

  const onChange: TableProps<DataType>["onChange"] = (
    pagination,
    filters,
    sorter,
    extra
  ) => {
    console.log("params", pagination, filters, sorter, extra);
  };
  return (
    <div>
      <div className="ml-6 mt-11 pr-28">
        <p className="text-orange-500 text-2xl font-bold text-left mb-4">
          Quản lý cấp số
        </p>
        <div className="flex justify-between gap-4 mb-4">
          <div>
            <p className="text-left text-base font-semibold mb-1">
              Chọn thời gian
            </p>
            <Space direction="vertical">
              <div className="flex items-center gap-2">
                <DatePicker />
                <p className="font-bold text-sm">{">"}</p>
                <DatePicker />
              </div>
            </Space>
          </div>
        </div>
        <Table
          className=""
          columns={columns}
          dataSource={renderSeri()}
          onChange={handleChange}
        />
        {/* rederUserData()  */}
        {/* <NavLink to={"/"}> */}
        <img
          style={{ top: 235 }}
          className="absolute  right-0 cursor-pointer"
          src={Url_Pb + "/img/reportmanager/Component 1.png"}
          alt=""
        />
        {/* </NavLink> */}
      </div>
    </div>
  );
}
