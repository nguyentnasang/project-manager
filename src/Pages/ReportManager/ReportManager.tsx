import React from "react";
import HeaderInfor from "../../component/HeaderInfor";
import TableReportManager from "./TableReportManager";

export default function ReportManager() {
  return (
    <div>
      <header className="flex justify-between pt-7 ml-7">
        <div className="flex text-xl text-gray-500 font-bold">
          <p className=" cursor-pointer mr-4">Báo cáo</p>
          <p>{">"}</p>

          <p className=" cursor-pointer text-orange-500 ml-4">Lập báo cáo</p>
        </div>
        <div>
          <HeaderInfor />
        </div>
      </header>
      <TableReportManager />
    </div>
  );
}
