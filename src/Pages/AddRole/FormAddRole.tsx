import React, { useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import firebase from "firebase/compat/app";
import { firebaseConfig } from "../../firebase/firebase";
import { message } from "antd";

export default function FormAddRole() {
  const navigate = useNavigate();
  const [nameRole, setNameRole] = useState("");
  const [describe, setDescribe] = useState("");
  const [isCheckedA1, setIsCheckedA1] = useState(false);
  const [isCheckedA2, setIsCheckedA2] = useState(false);
  const [isCheckedA3, setIsCheckedA3] = useState(false);
  const [isCheckedA4, setIsCheckedA4] = useState(false);
  const [isCheckedB1, setIsCheckedB1] = useState(false);
  const [isCheckedB2, setIsCheckedB2] = useState(false);
  const [isCheckedB3, setIsCheckedB3] = useState(false);
  const [isCheckedB4, setIsCheckedB4] = useState(false);
  const formRole = {
    nameRole,
    describe,
    isCheckedA1,
    isCheckedA2,
    isCheckedA3,
    isCheckedA4,
    isCheckedB1,
    isCheckedB2,
    isCheckedB3,
    isCheckedB4,
  };
  firebase.initializeApp(firebaseConfig);
  const database = firebase.database();
  const AddRole = () => {
    const roleRef = database.ref("roles");
    if (nameRole.length == 0) {
      message.warning("vui lòng nhập tên vai trò");
      return;
    } else if (
      !isCheckedA1 &&
      !isCheckedA2 &&
      !isCheckedA3 &&
      !isCheckedA4 &&
      !isCheckedB1 &&
      !isCheckedB2 &&
      !isCheckedB3 &&
      !isCheckedB4
    ) {
      message.warning("vui lòng chọn chức năng");
      return;
    } else {
      roleRef
        .orderByChild("nameRole") //sắp xếp các nút con, để tìm kiếm các nút con có thuộc tính "nameRole" bằng giá trị của biến nameRole
        .equalTo(nameRole) //sử dụng equalTo để tìm kiếm nút con với thuộc tính nameRole bằng với giá trị của biến nameRole được gửi từ form.
        .once("value") //lắng nghe sự kiện "value" để nhận kết quả trả về.
        .then((snapshot) => {
          if (snapshot.exists()) {
            //snapshot.exists() Nó được sử dụng để kiểm tra xem có tồn tại nút dữ liệu hay không. nó trả về giá trị true false
            message.error("Tên vai trò đã tồn tại trong hệ thống.");
          } else {
            // Thêm vai trò mới nếu nameRole không tồn tại
            roleRef
              .push(formRole)
              .then((res) => {
                message.success("Thêm vai trò thành công.");
                navigate("/rolemanager");
              })
              .catch((err) => {
                message.error("Thêm vai trò thất bại.");
              });
          }
        })
        .catch((err) => {
          console.error(err);
          message.error("Đã có lỗi xảy ra, vui lòng thử lại sau.");
        });
    }
  };
  return (
    <div className="mx-6 mb-4">
      <div className="bg-white rounded-2xl px-6 py-4">
        <p className="text-left text-orange-500 font-bold text-xl">
          Thông tin vai trò
        </p>
        <div className="grid grid-cols-2">
          <div>
            <span className="flex font-semibold mt-3 mb-2">
              <p>Tên vai trò</p>
              <p className="text-red-500 ml-1">*</p>
            </span>
            <input
              onChange={(e) => setNameRole(e.target.value)}
              style={{ width: 560 }}
              className="w border-2 border-gray-100 rounded-lg py-1 pl-3 flex"
              // onChange={(e) => setUserName(e.target.value)}
              type="text"
              placeholder="Nhập tên vai trò"
            />
            <span className="flex font-semibold mt-3 mb-2">
              <p>Mô tả:</p>
            </span>
            <textarea
              onChange={(e) => setDescribe(e.target.value)}
              style={{ width: 560, height: 160, verticalAlign: "top" }}
              className="w border-2 border-gray-100 rounded-lg pl-3 flex"
              // onChange={(e) => setUserName(e.target.value)}
              placeholder="Nhập mô tả"
            ></textarea>
            <span className="flex items-center mt-4 text-sm font-normal ">
              <p className="text-red-500 ml-1 mr-1">*</p>
              <p className="text-gray-400">Là trường thông tin bắt buộc</p>
            </span>
          </div>
          <div className="">
            <span className="flex font-semibold mt-3 mb-2">
              <p>Phân quyền chức năng</p>
              <p className="text-red-500 ml-1">*</p>
            </span>
            <div className="rounded-lg bg-orange-50 text-left pl-6 pt-4 pb-8">
              {/* //Nhóm chức năng AAAAAAAAAAAAAAAAAA */}
              <p className="text-xl text-orange-500 font-bold">
                Nhóm chức năng A
              </p>
              <div className="my-3 flex">
                <input
                  checked={isCheckedA1}
                  onChange={() => {
                    setIsCheckedA1(!isCheckedA1);
                  }}
                  className="mr-2"
                  type="checkbox"
                />

                <p>Tất cả</p>
              </div>
              <div className="my-3 flex">
                <input
                  checked={isCheckedA2}
                  onChange={() => {
                    setIsCheckedA2(!isCheckedA2);
                  }}
                  className="mr-2"
                  type="checkbox"
                />

                <p>Chức năng x</p>
              </div>
              <div className="my-3 flex">
                <input
                  checked={isCheckedA3}
                  onChange={() => {
                    setIsCheckedA3(!isCheckedA3);
                  }}
                  className="mr-2"
                  type="checkbox"
                />

                <p>Chức năng y</p>
              </div>
              <div className="my-3 flex">
                <input
                  checked={isCheckedA4}
                  onChange={() => {
                    setIsCheckedA4(!isCheckedA4);
                  }}
                  className="mr-2"
                  type="checkbox"
                />

                <p>Chức năng z</p>
              </div>
              {/* //Nhóm chức năng BBBBBBBBBBBBBBBBBB */}

              <p className="text-xl text-orange-500 font-bold">
                Nhóm chức năng B
              </p>
              <div className="my-3 flex">
                <input
                  checked={isCheckedB1}
                  onChange={() => {
                    setIsCheckedB1(!isCheckedB1);
                  }}
                  className="mr-2"
                  type="checkbox"
                />

                <p>Tất cả</p>
              </div>
              <div className="my-3 flex">
                <input
                  checked={isCheckedB2}
                  onChange={() => {
                    setIsCheckedB2(!isCheckedB2);
                  }}
                  className="mr-2"
                  type="checkbox"
                />

                <p>Chức năng x</p>
              </div>
              <div className="my-3 flex">
                <input
                  checked={isCheckedB3}
                  onChange={() => {
                    setIsCheckedB3(!isCheckedB3);
                  }}
                  className="mr-2"
                  type="checkbox"
                />

                <p>Chức năng y</p>
              </div>
              <div className="my-3 flex">
                <input
                  checked={isCheckedB4}
                  onChange={() => {
                    setIsCheckedB4(!isCheckedB4);
                  }}
                  className="mr-2"
                  type="checkbox"
                />

                <p>Chức năng z</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div style={{ bottom: -80 }} className="flex justify-center w-full mt-6">
        <NavLink to={"/rolemanager"}>
          <button className="bg-orange-50 border-2 border-orange-400 text-orange-400 px-14 py-3 rounded-lg mr-8">
            Hủy bỏ
          </button>
        </NavLink>
        <button
          onClick={() => {
            AddRole();
          }}
          className="bg-orange-500 text-white px-14 py-3 rounded-lg"
        >
          Thêm
        </button>
      </div>
    </div>
  );
}
