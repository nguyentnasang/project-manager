import React from "react";
import HeaderInfor from "../../component/HeaderInfor";
import FormAddRole from "./FormAddRole";

export default function AddRole() {
  return (
    <div className="bg-gray-50 h-full">
      {" "}
      <header className="flex justify-between pt-7 ml-7">
        <div className="flex text-xl text-gray-500 font-bold">
          <p className=" cursor-pointer mr-4">Cài đặt hệ thống</p>
          <p>{">"}</p>
          <p className=" cursor-pointer mr-4 ml-4">Quản lý vai trò</p>
          <p>{">"}</p>
          <p className=" cursor-pointer text-orange-500 ml-4">Thêm vai trò</p>
        </div>
        <div>
          <HeaderInfor />
        </div>
      </header>
      <div className="text-left mt-9 ml-6 mb-3">
        <span className="text-orange-500 text-2xl font-bold ">
          Danh sách vai trò
        </span>
        <div className="mr-6"></div>
      </div>
      <FormAddRole />
    </div>
  );
}
