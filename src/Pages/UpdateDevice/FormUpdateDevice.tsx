import { Button, Form, Input, Select, message } from "antd";
import { Option } from "antd/es/mentions";
import React, { useEffect, useState } from "react";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import firebase from "firebase/compat/app";
import { firebaseConfig } from "../../firebase/firebase";
import { useSelector } from "react-redux";
interface Device {
  IP: string;
  codeDevice: string;
  nameDevice: string;
  password: string;
  typeDevice: string;
  useDevice: any;
  userName: string;
}
export default function FormUpdateDevice() {
  const [dataDivice, setDataDivice] = useState<Device>({
    IP: "",
    codeDevice: "",
    nameDevice: "",
    password: "",
    typeDevice: "",
    useDevice: "",
    userName: "",
  });
  const userLogin = useSelector((state: any) => state.userSlice.userLogin);
  const navigate = useNavigate();
  const param = useParams().id;
  firebase.initializeApp(firebaseConfig);
  const database = firebase.database();
  const onFinish = (values: any) => {
    console.log("values", values);
    database
      .ref(`devices/${param}`)
      .update(values)
      .then((res) => {
        message.success("Cập nhật thiết bị thành công");
        //activities
        database.ref("activities").push({
          userName: userLogin.fullName,
          dateTime: `${new Date()}`,
          IP: values.IP,
          operation: "Cập nhật thiết bị",
        });
        navigate("/devicemanager");
      })
      .catch((err) => {
        message.error("Cập nhật thiết bị thất bại");
      });
  };
  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };
  useEffect(() => {
    database
      .ref(`devices/${param}`)
      .once("value")
      .then((snapshot) => {
        const deviceData = snapshot.val();
        setDataDivice(deviceData);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  return (
    <div>
      <Form
        fields={[
          {
            name: ["IP"],
            value: dataDivice.IP,
          },
          {
            name: ["codeDevice"],
            value: dataDivice.codeDevice,
          },
          {
            name: ["nameDevice"],
            value: dataDivice.nameDevice,
          },
          {
            name: ["password"],
            value: dataDivice.password,
          },
          {
            name: ["typeDevice"],
            value: dataDivice.typeDevice,
          },
          {
            name: ["useDevice"],
            value: dataDivice.useDevice,
          },
          {
            name: ["userName"],
            value: dataDivice.userName,
          },
        ]}
        name="basic"
        // style={{ maxWidth: 600 }}
        className="mt-4"
        layout="vertical"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <div className="bg-white rounded-2xl pt-4 px-6 pb-24 mb-5">
          <span className="text-orange-500 font-bold text-xl">
            Thông tin thiết bị
          </span>
          <div className="grid grid-cols-2 gap-6 mt-5">
            <div>
              <Form.Item
                label={
                  <div className="flex">
                    <p className="font-semibold">Mã thiết bị</p>
                    <p className="text-red-500 ml-1 text-xl">*</p>
                  </div>
                }
                name="codeDevice"
                rules={[
                  { required: true, message: "vui lòng nhập mã thiết bị!" },
                ]}
              >
                <Input placeholder="Nhập mã thiết bị" />
              </Form.Item>
              <Form.Item
                label={
                  <div className="flex">
                    <p className="font-semibold">Tên thiết bị:</p>
                    <p className="text-red-500 ml-1 text-xl">*</p>
                  </div>
                }
                name="nameDevice"
                rules={[
                  { required: true, message: "Vui lòng nhập tên thiết bị!" },
                ]}
              >
                <Input placeholder="Nhập tên thiết bị" />
              </Form.Item>
              <Form.Item
                label={
                  <div className="flex">
                    <p className="font-semibold">Địa chỉ IP:</p>
                    <p className="text-red-500 ml-1 text-xl">*</p>
                  </div>
                }
                name="IP"
                rules={[
                  { required: true, message: "Vui lòng nhập địa chỉ IP!" },
                ]}
              >
                <Input placeholder="Nhập địa chỉ IP" />
              </Form.Item>
            </div>
            <div>
              <Form.Item
                name="typeDevice"
                label={
                  <div className="flex">
                    <p className="font-semibold">Loại thiết bị:</p>
                    <p className="text-red-500 ml-1 text-xl">*</p>
                  </div>
                }
                hasFeedback
                rules={[
                  { required: true, message: "Vui lòng chọn loại thiết bị!" },
                ]}
              >
                <Select placeholder="Chọn loại thiết bị">
                  <Option value="Kiosk">Kiosk</Option>
                  <Option value="Display counter">Display counter</Option>
                </Select>
              </Form.Item>
              <Form.Item
                label={
                  <div className="flex">
                    <p className="font-semibold">Tên đăng nhập:</p>
                    <p className="text-red-500 ml-1 text-xl">*</p>
                  </div>
                }
                name="userName"
                rules={[
                  { required: true, message: "Vui lòng Nhập tài khoản!" },
                ]}
              >
                <Input disabled placeholder="Nhập tài khoản" />
              </Form.Item>
              <Form.Item
                label={
                  <div className="flex">
                    <p className="font-semibold">Mật khẩu:</p>
                    <p className="text-red-500 ml-1 text-xl">*</p>
                  </div>
                }
                name="password"
                rules={[
                  { required: true, message: "Vui lòng nshập mật khẩu!" },
                ]}
              >
                <Input.Password disabled placeholder="Nhập mật khẩu" />
              </Form.Item>
            </div>
          </div>
          <Form.Item
            name="useDevice"
            label={
              <div className="flex">
                <p className="font-semibold">Dịch vụ sử dụng:</p>
                <p className="text-red-500 ml-1 text-xl">*</p>
              </div>
            }
            rules={[
              {
                required: true,
                message: "Vui lòng chọn dịch vụ sử dụng!",
                type: "array",
              },
            ]}
          >
            <Select mode="multiple" placeholder="Nhập dịch vụ sử dụng">
              <Option value="Tất cả">Tất cả</Option>
              <Option value="Khám răng hàm mặt">Khám răng hàm mặt</Option>
              <Option value="Khám tai mũi họng">Khám tai mũi họng</Option>
            </Select>
          </Form.Item>
          <span className="flex items-center mt-4 text-sm font-normal ">
            <p className="text-red-500 ml-1 mr-1">*</p>
            <p className="text-gray-400">Là trường thông tin bắt buộc</p>
          </span>
        </div>
        <Form.Item>
          <div className="flex text-center justify-center">
            <NavLink to={"/devicemanager"}>
              <button className="bg-orange-50 border-2 border-orange-400 text-orange-400 px-14 py-3 rounded-lg mr-8">
                Hủy bỏ
              </button>
            </NavLink>
            <Button
              className="bg-orange-500 text-white px-10 py-6 rounded-lg flex items-center"
              htmlType="submit"
            >
              Cập nhật
            </Button>
          </div>
        </Form.Item>
      </Form>
    </div>
  );
}
