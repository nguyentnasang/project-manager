import React from "react";
import HeaderInfor from "../../component/HeaderInfor";
import FormUpdateDevice from "./FormUpdateDevice";

export default function UpdateDevice() {
  return (
    <div className="bg-gray-100 h-full">
      <header className="flex justify-between pt-7 ml-7">
        <div className="flex text-xl text-gray-500 font-bold">
          <p className=" cursor-pointer mr-4">Thiết bị</p>
          <p>{">"}</p>
          <p className=" cursor-pointer mr-4 ml-4">Danh sách thiết bị</p>
          <p>{">"}</p>
          <p className=" cursor-pointer text-orange-500 ml-4">
            Cập nhật thiết bị
          </p>
        </div>
        <div>
          <HeaderInfor />
        </div>
      </header>
      <div className="text-left mt-9 ml-6 mb-3">
        <span className="text-orange-500 text-2xl font-bold ">
          Quản lý thiết bị
        </span>
        <div className="mr-6">
          <FormUpdateDevice />
        </div>
      </div>
    </div>
  );
}
