import React, { useEffect, useState } from "react";
import { message } from "antd";
import { Link, NavLink, useNavigate } from "react-router-dom";
import { Url_Pb } from "../../utils/Utils_Url";
// firebase
import firebase from "firebase/compat/app";
import "firebase/compat/database";
// file cấu hình
import "../../firebase/firebase";
import { firebaseConfig } from "../../firebase/firebase";
import { useDispatch } from "react-redux";
import { setUserLogin } from "../../redux-toolkit/userSlice";
import { userLocalService } from "../../Service/localService";

export default function Login() {
  useEffect(() => {
    message.info("Demo tk:1, mk:1");
  }, []);

  const dispath = useDispatch();
  const navigate = useNavigate();

  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  // icon eye password
  const [isPasswordVisible, setIsPasswordVisible] = useState<boolean>(false);

  const togglePasswordVisibility = () => {
    setIsPasswordVisible(!isPasswordVisible);
  };
  // validate
  const [isLogin, setIsLogin] = useState<boolean>(true);
  const validateButton = () => {
    if (!isLogin) {
      return "red 2px solid";
    } else {
      return "";
    }
  };
  const validateWrong = () => {
    if (isLogin) {
      return "hidden";
    } else {
      return "top-40";
    }
  };
  const validateForget = () => {
    if (isLogin) {
      return `top-40`;
    } else {
      return "top-64 left-36 ";
    }
  };
  firebase.initializeApp(firebaseConfig);
  const handleLogin = (userName: string, password: string) => {
    firebase
      .database()
      .ref("users")
      .orderByChild("userName")
      .equalTo(userName)
      .once("value")
      .then((snapshot: any) => {
        const user = snapshot.val() as { [key: string]: any }; // định kiểu user
        console.log("snapshot", snapshot.val());
        console.log("user ", user); // debug
        // Lấy giá trị userData đầu tiên
        const userData = Object.values(user)[0];

        if (userData && userData.password === password) {
          // Đăng nhập thành công
          message.success("đăng nhập thành công");
          userLocalService.set(userData);
          dispath(setUserLogin(userData));
          setIsLogin(true);
          // Duy chuyển đến trang khác
          setTimeout(() => {
            navigate("/infor");
          }, 1000);
        } else {
          message.error("Sai tên đăng nhập hoặc mật khẩu");
          message.warning("demo tk:1 mk:1");
          setIsLogin(false);
        }
      })
      .catch((error: any) => {
        console.log(error);
        message.error("Tài khoản không hợp lệ");
        message.warning("demo tk:1 mk:1");

        setIsLogin(false);
      });
  };
  const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter") {
      event.preventDefault();
      handleLogin(userName, password);
    }
  };

  return (
    <div className="grid grid-cols-10 top h-screen ">
      <div style={{ background: "#F6F6F6" }} className=" col-span-4 h-full">
        <h1 className="text-6xl font-bold text-orange-500 mt-20 mb-14">
          SMedi@
        </h1>
        <div style={{ width: 400 }} className="flex flex-col mx-auto relative">
          <p className="text-left mb-1 text-lg">Tên đăng nhập *</p>
          <input
            onKeyDown={handleKeyPress}
            style={{ border: validateButton() }}
            type="text"
            className={`rounded h-11 pl-3 text-lg `}
            onChange={(event) => setUserName(event.target.value)}
          />
          <p className="text-left mb-1 text-lg">Mật khẩu *</p>
          <div className="relative">
            <input
              onKeyDown={handleKeyPress}
              style={{ border: validateButton() }}
              type={isPasswordVisible ? "text" : "password"}
              className="rounded h-11 pl-3 text-lg w-full"
              onChange={(event) => setPassword(event.target.value)}
            ></input>
            <img
              style={{ top: 14 }}
              className="w-4 absolute right-3"
              src={Url_Pb + "./img/Login/Vector.png"}
              alt=""
              onClick={togglePasswordVisibility}
            />
          </div>
          {/* quên mk  */}
          <NavLink to="/login/forgot">
            <p
              style={{}}
              className={`text-left absolute text-red-500 text-sm  ${validateForget()}`}
            >
              Quên mật khẩu?
            </p>
          </NavLink>
          {/* sai mk  */}
          <div
            className={`text-red-500 flex text-sm absolute ${validateWrong()}`}
          >
            <p
              style={{
                fontSize: "13px",
              }}
              className=" rounded-full border-2 border-red-500 px-2 mr-1 flex justify-center items-center font-bold"
            >
              !
            </p>
            <p>Sai mật khẩu hoặc tên đăng nhập</p>
          </div>
          <button
            onClick={() => {
              // setIsLogin(false);
              handleLogin(userName, password);
              console.log("password", password);
              console.log("userName", userName);
            }}
            className="text-base bg-orange-400 text-white w-40 h-10 rounded-md mx-auto mt-14 hover:brightness-75 "
          >
            Đăng nhập
          </button>
        </div>
      </div>
      <div className=" col-span-6 h-screen my-auto p-20 relative">
        <img width={500} src={Url_Pb + "/img/login/Group 341.png"} alt="" />
        <div className="text-orange-400 absolute right-8 top-80 text-left">
          <p className="text-4xl">Hệ thống</p>
          <p className="font-bold text-5xl">QUẢN LÝ XẾP HÀNG</p>
        </div>
      </div>
    </div>
  );
}
