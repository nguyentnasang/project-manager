import React, { useEffect, useState } from "react";
import HeaderInfor from "../../component/HeaderInfor";
import { NavLink, useParams } from "react-router-dom";
import { Url_Pb } from "../../utils/Utils_Url";
import firebase from "firebase/compat/app";
import { firebaseConfig } from "../../firebase/firebase";
interface Device {
  codeDevice: string;
  nameDevice: string;
  IP: string;
  typeDevice: string;
  userName: string;
  password: string;
  useDevice: any;
}
export default function DetailDevice() {
  const param = useParams().id;
  const [dataDevide, setDataDevice] = useState<Device>({
    codeDevice: "",
    nameDevice: "",
    IP: "",
    typeDevice: "",
    userName: "",
    password: "",
    useDevice: [],
  });
  console.log("dataDevideeeeeee", dataDevide.useDevice);
  console.log("dataDevide", dataDevide);
  firebase.initializeApp(firebaseConfig);
  const db = firebase.database();
  useEffect(() => {
    db.ref(`devices/${param}`).once("value", (snapshot) => {
      setDataDevice(snapshot.val());
    });
  }, []);

  return (
    <div className="bg-gray-50 h-full">
      {" "}
      <header className="flex justify-between pt-7 ml-7">
        <div className="flex text-xl text-gray-500 font-bold">
          <p className=" cursor-pointer mr-4">Cài đặt hệ thống</p>
          <p>{">"}</p>
          <p className=" cursor-pointer mr-4 ml-4">Quản lý vai trò</p>
          <p>{">"}</p>
          <p className=" cursor-pointer text-orange-500 ml-4">
            Chi tiết thiết bị
          </p>
        </div>
        <div>
          <HeaderInfor />
        </div>
      </header>
      <div className="text-left mt-9 ml-6 mb-3 pr-24">
        <span className="text-orange-500 text-2xl font-bold ">
          Quản lý thiết bị
        </span>
        <div className="mr-6 bg-white rounded-lg pt-4 pl-6 pb-80 mt-4">
          <span className="text-orange-500 font-bold text-xl">
            Thông tin thiết bị
          </span>
          <div className="grid grid-cols-2 mt-5">
            <div className="grid grid-cols-4">
              <div className="col-span-1">
                <p className="text-base font-semibold">Mã thiết bị:</p>
                <p className="text-base font-semibold py-5">Tên thiết bị:</p>
                <p className="text-base font-semibold">Địa chỉ IP:</p>
              </div>
              <div className="col-span-2">
                <p>{dataDevide.codeDevice}</p>
                <p className="py-5">{dataDevide.nameDevice}</p>
                <p>{dataDevide.IP}</p>
              </div>
            </div>
            <div className="grid grid-cols-4">
              <div className="col-span-1">
                <p className="text-base font-semibold">Loại thiết bị:</p>
                <p className="text-base font-semibold py-5">Tên đăng nhập:</p>
                <p className="text-base font-semibold">Mật khẩu:</p>
              </div>
              <div className="col-span-3">
                <p>{dataDevide.typeDevice}</p>
                <p className="py-5">{dataDevide.userName}</p>
                <p>{dataDevide.password}</p>
              </div>
            </div>
          </div>
          <div>
            <p className="text-base font-semibold mt-4 mb-2">
              Dịch vụ sử dụng:
            </p>
            <p>
              {dataDevide?.useDevice?.map(
                (item: any, index: number, array: any[]) => {
                  if (index === array.length - 1) {
                    // kiểm tra phần tử cuối cùng trong mảng
                    return `${item}.`; // thay dấu phẩy bằng dấu chấm
                  }
                  return `${item}, `;
                }
              )}
            </p>
          </div>
        </div>
      </div>
      <NavLink to={`/devicemanager/update/${param}`}>
        <img
          style={{ top: 150 }}
          className="absolute  right-0 cursor-pointer"
          src={Url_Pb + "/img/detaildevice/Component 1.png"}
          alt=""
        />
      </NavLink>
    </div>
  );
}
