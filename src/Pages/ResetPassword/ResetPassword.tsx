import React, { useState } from "react";
import { Url_Pb } from "../../utils/Utils_Url";
import { useSelector } from "react-redux";
import { UserSliceState } from "../../redux-toolkit/userSlice";
import { message } from "antd";
import { useNavigate } from "react-router";
import firebase from "firebase/compat/app";
import "firebase/compat/database";
import { firebaseConfig } from "../../firebase/firebase";
interface RootState {
  userSlice: UserSliceState;
}
interface UserChange {
  $key: string;

  // Các thuộc tính khác nếu có
}
export default function ResetPassword() {
  const navigate = useNavigate();
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const userChange = useSelector(
    (state: RootState) => state.userSlice.userChange
  );
  // console.log("userChange", userChange);
  const objKey = Object.keys(userChange)[0]; // "-NRxIkGc45dB2aO5p2rJ"
  // console.log(objKey);
  // firebase
  firebase.initializeApp(firebaseConfig);
  const updatePassword = (objKey: any, password: any) => {
    if (password && password === confirmPassword) {
      const databaseRef = firebase.database().ref("users/" + objKey);
      databaseRef.update({ password: password });
      message.success("cập nhật mật khẩu thành công vui lòng đăng nhập lại");
      setTimeout(() => {
        navigate("/");
      }, 1000);
    } else {
      message.warning("không để trống và nhập trùng mật khẩu");
    }
  };

  return (
    <div className="grid grid-cols-10 top h-screen ">
      <div style={{ background: "#F6F6F6" }} className=" col-span-4 h-full">
        <h1 className="text-6xl font-bold text-orange-500 mt-20 mb-14">
          SMedi@
        </h1>
        <div>
          <p className="text-2xl font-bold mb-3">Đặt lại mật khẩu mới</p>
          <div style={{ width: 400 }} className="m-auto">
            <p className="text-left text-lg mb-1">Mật khẩu</p>
            <input
              onChange={(event) => setPassword(event.target.value)}
              className="w-full h-11 rounded-lg"
              type="text"
            />
            <p className="text-left text-lg mb-1 mt-4">Nhập lại mật khẩu</p>
            <input
              onChange={(event) => setConfirmPassword(event.target.value)}
              className="w-full h-11 rounded-lg"
              type="text"
            />
          </div>
          <button
            onClick={() => {
              updatePassword(objKey, password);
            }}
            className="bg-orange-400 rounded-lg py-2 px-12 mt-12"
          >
            Xác nhận
          </button>
        </div>
      </div>
      <div className="w-full col-span-6 pl-11 my-auto">
        <img src={Url_Pb + "/img/login/Frame.png"} alt="" />
      </div>
    </div>
  );
}
