import React, { useState } from "react";
import { Url_Pb } from "../../utils/Utils_Url";
import { NavLink, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux/es/exports";
import {
  equalTo,
  get,
  getDatabase,
  orderByChild,
  query,
  ref,
} from "firebase/database";
import "../../firebase/firebase";
import { message } from "antd";
import { setUserChange } from "../../redux-toolkit/userSlice";
export default function ForgotPassword() {
  const navigate = useNavigate();
  const dispath = useDispatch();
  const [email, setEmail] = useState("");
  // firebase
  const checkEmail = () => {
    const db = getDatabase();
    const usersRef = ref(db, "users");
    const queryRef = query(usersRef, orderByChild("email"), equalTo(email));
    const queryRefEmail = query(
      usersRef,
      orderByChild("email"),
      equalTo(email)
    );
    get(queryRef).then((snapshot) => {
      if (snapshot.exists()) {
        const user = snapshot.val(); // lấy đối tượng người dùng
        // const userData = Object.values(user)[0]; //lấy giá trị user đầu tiên

        // lưu người dùng lên redux
        dispath(setUserChange(user));
        console.log("user", user);
        message.success("Xác nhận email thành công");
        setTimeout(() => {
          navigate("/login/forgot/reset");
        }, 1000);
      } else {
        message.error("Email không tồn tại trong hệ thống!");
      }
    });
  };
  return (
    <div className="grid grid-cols-10 top h-screen">
      <div style={{ background: "#F6F6F6" }} className=" col-span-4 h-full">
        <h1 className="text-6xl font-bold text-orange-500 mt-20 mb-14">
          SMedi@
        </h1>
        <div>
          <p className="text-2xl font-bold">Đặt lại mật khẩu</p>
          <p className="text-lg mt-2">
            Vui lòng nhập email để đặt lại mật khẩu của bạn *
          </p>
          <input
            onChange={(e) => setEmail(e.target.value)}
            style={{ width: 400, height: 44 }}
            className="rounded-lg text-lg pl-3"
            type="text"
          />
          <div className="mt-12">
            <NavLink to={"/"}>
              <button className="px-14 py-2 rounded-md text-orange-400 border-2 border-orange-400">
                Hủy
              </button>
            </NavLink>
            <button
              onClick={() => {
                checkEmail();
              }}
              className="px-12 py-2 rounded-md ml-6 bg-orange-400 text-white"
            >
              Tiếp tục
            </button>
          </div>
        </div>
      </div>
      <div className="w-full col-span-6 pl-11 my-auto">
        <img src={Url_Pb + "/img/login/Frame.png"} alt="" />
      </div>
    </div>
  );
}
