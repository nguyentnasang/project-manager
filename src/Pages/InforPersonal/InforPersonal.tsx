import React from "react";
import HeaderInfor from "../../component/HeaderInfor";
import FormInfoPersonal from "./FormInfoPersonal";

export default function InforPersonal() {
  return (
    <div className="bg-gray-50 h-full">
      <header className="flex justify-between pt-7 ml-7">
        <div className="flex text-xl font-bold text-orange-500">
          <p className=" cursor-pointer mr-4">Thông tin cá nhân</p>
        </div>
        <div>
          <HeaderInfor />
        </div>
      </header>
      <FormInfoPersonal />
    </div>
  );
}
