import React from "react";
import { Url_Pb } from "../../utils/Utils_Url";
import { useSelector } from "react-redux";
import { UserSliceState } from "../../redux-toolkit/userSlice";

export default function FormInfoPersonal() {
  interface RootState {
    userSlice: UserSliceState;
  }

  const userLogin = useSelector((state: any) => state.userSlice.userLogin);
  // const userData: any = Object.values(userLogin)[0];
  if (typeof userLogin === "object" && userLogin !== null) {
    console.log("userLogin", userLogin);
  } else {
    console.log("userLogin is not an object");
  }
  return (
    <div className="bg-white rounded-xl mt-28 ml-6 mr-28 flex pt-10">
      <div className="flex flex-col  ml-6 relative">
        <img
          width={248}
          src={Url_Pb + "/img/inforPersonal/unsplash_Fyl8sMC2j2Q.png"}
          alt=""
        />
        <p className="text-gray-800 font-bold text-2xl mt-5 mb-14">
          {userLogin.fullName}
        </p>
        <img
          className="absolute bottom-28 right-6"
          width={45}
          src={Url_Pb + "/img/inforPersonal/Group 624817.png"}
          alt=""
        />
      </div>
      <div className="flex ml-6">
        <div className="mr-6">
          <p className="text-left font-semibold mb-2">Tên người dùng</p>
          <input
            disabled
            value={userLogin.fullName}
            className="bg-gray-100 w-96  rounded-md py-2 pl-4 text-sm"
            type="text"
          />
          <p className="text-left font-semibold mb-2 mt-6">Số điện thoại </p>
          <input
            disabled
            value={userLogin.phoneNumber}
            className="bg-gray-100 w-96  rounded-md py-2 pl-4 text-sm"
            type="text"
          />
          <p className="text-left font-semibold mb-2 mt-6">Email:</p>
          <input
            disabled
            value={userLogin.email}
            className="bg-gray-100 w-96  rounded-md py-2 pl-4 text-sm"
            type="text"
          />
        </div>
        <div>
          <p className="text-left font-semibold mb-2">Tên đăng nhập </p>
          <input
            disabled
            value={userLogin.userName}
            className="bg-gray-100 w-96  rounded-md py-2 pl-4 text-sm"
            type="text"
          />
          <p className="text-left font-semibold mb-2  mt-6">Mật khẩu</p>
          <input
            disabled
            value={userLogin.password}
            className="bg-gray-100 w-96  rounded-md py-2 pl-4 text-sm"
            type="text"
          />
          <p className="text-left font-semibold mb-2 mt-6"> Vai trò:</p>
          <input
            disabled
            value={userLogin.status}
            className="bg-gray-100 w-96  rounded-md py-2 pl-4 text-sm"
            type="text"
          />
        </div>
      </div>
    </div>
  );
}
