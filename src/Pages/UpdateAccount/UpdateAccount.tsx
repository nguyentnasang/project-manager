import React from "react";
import FormUpdateAccount from "./FormUpdateAccount";
import HeaderInfor from "../../component/HeaderInfor";

export default function UpdateAccount() {
  return (
    <div className="bg-gray-50 h-full">
      <header className="flex justify-between pt-7 ml-7">
        <div className="flex text-xl text-gray-500 font-bold">
          <p className=" cursor-pointer mr-4">Cài đặt hệ thống</p>
          <p>{">"}</p>
          <p className=" cursor-pointer mr-4 ml-4">Quản lý tài khoản</p>
          <p>{">"}</p>
          <p className=" cursor-pointer text-orange-500 ml-4">
            Cập nhật tài khoản
          </p>
        </div>
        <div>
          <HeaderInfor />
        </div>
      </header>
      <p className="text-left ml-6 mt-11 text-2xl mb-3 text-orange-500 font-bold">
        Quản lý tài khoản
      </p>
      <FormUpdateAccount />
    </div>
  );
}
