import React, { useEffect, useState } from "react";
import { NavLink, useNavigate, useParams } from "react-router-dom";
import firebase from "firebase/compat/app";
import { firebaseConfig } from "../../firebase/firebase";
import { message } from "antd";
interface User {
  fullName: string;
  phoneNumber: string;
  email: string;
  status: string;
  userName: string;
  password: string;
  role: string;
}
export default function FormUpdateAccount() {
  const navigate = useNavigate();
  const param = useParams<{ id: string }>();

  const [userUpdate, setUserUpdate] = useState<User>({
    fullName: "",
    phoneNumber: "",
    email: "",
    status: "",
    userName: "",
    password: "",
    role: "",
  });
  console.log("userUpdate", userUpdate.fullName);
  const [fullName, setFullName] = useState("");
  const [userName, setUserName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [role, setRole] = useState("");
  const [status, setStatus] = useState("");
  const [dataRole, setDataRole] = useState<any>([]);
  const formData = {
    fullName,
    userName,
    phoneNumber,
    password,
    email,
    confirmPassword,
    role,
    status,
  };
  console.log("formData", formData);
  // console.log("param", param.id);;
  firebase.initializeApp(firebaseConfig);
  // truy cập vào database
  const database = firebase.database();
  //lấy danh sách dữ liệu có khóa chính là userid
  useEffect(() => {
    const userId = param.id;
    database
      .ref(`users/${userId}`)
      .once("value")
      .then((snapshot) => {
        const userData = snapshot.val();
        setUserUpdate(userData);
        // console.log("userData", userData);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  useEffect(() => {
    database
      .ref("roles")
      .once("value")
      .then((res) => {
        setDataRole(res.val());
      })
      .catch((err) => {});
  }, []);
  const arrRole = Object?.values(dataRole);
  console.log("arrRole", arrRole);
  const renderOptionRole = arrRole?.map((item: any) => (
    <option className="" value={item.nameRole} key={item.nameRole}>
      {item.nameRole}
    </option>
  ));
  const handleUpdateUser = () => {
    // update giá trị user
    const UserId = param.id;
    const userRef = database.ref(`users/${UserId}`);
    userRef
      .update(formData)
      .then((res) => {
        message.success("update tài khoản thành công");
        navigate("/accountmanager");
      })
      .catch((err) => {
        message.error("update tài khoản thất bại");
      });
  };

  useEffect(() => {
    setFullName(userUpdate.fullName);
    setUserName(userUpdate.userName);
    setPhoneNumber(userUpdate.phoneNumber);
    setPassword(userUpdate.password);
    setEmail(userUpdate.email);
    setConfirmPassword(userUpdate.password);
    setRole(userUpdate.role);
    setStatus(userUpdate.status);
  }, [userUpdate]);
  return (
    <div className="px-6">
      <div className=" bg-white rounded-2xl pt-4">
        <p className="text-left ml-6 mb-5 text-xl text-orange-500 font-bold">
          Thông tin tài khoản
        </p>
        <div className="flex  px-6 pb-20">
          <div className="mr-6">
            <div className="flex mb-2">
              <p>Họ tên</p>
              <span className="text-red-500 ml-1">*</span>
            </div>
            <input
              defaultValue={userUpdate.fullName}
              style={{ width: 560 }}
              className="w border-2 border-gray-100 rounded-lg py-1 pl-3"
              onChange={(e) => setFullName(e.target.value)}
              type="text"
              placeholder="Nhập họ tên"
            />
            <div className="flex mb-2 mt-4">
              <p>Số điện thoại</p>
              <span className="text-red-500 ml-1">*</span>
            </div>
            <input
              defaultValue={userUpdate.phoneNumber}
              style={{ width: 560 }}
              className="w border-2 border-gray-100 rounded-lg py-1 pl-3"
              onChange={(e) => setPhoneNumber(e.target.value)}
              type="text"
              placeholder="Nhập số điện thoại"
            />
            <div className="flex mb-2 mt-4">
              <p>Email</p>
              <span className="text-red-500 ml-1">*</span>
            </div>
            <input
              defaultValue={userUpdate.email}
              style={{ width: 560 }}
              className="w border-2 border-gray-100 rounded-lg py-1 pl-3"
              onChange={(e) => setEmail(e.target.value)}
              type="text"
              placeholder="Nhập email"
            />
            <div className="flex mb-2 mt-4">
              <p>Vai trò</p>
              <span className="text-red-500 ml-1">*</span>
            </div>

            <select
              value={formData.status}
              onChange={(e) => setStatus(e.target.value)}
              style={{ width: 560 }}
              className="py-1 border-2 border-gray-100 rounded-lg pl-3"
            >
              {renderOptionRole}
            </select>
            <span className="flex items-center mt-4 text-sm font-normal ">
              <p className="text-red-500 ml-1 mr-1">*</p>
              <p className="text-gray-400">Là trường thông tin bắt buộc</p>
            </span>
          </div>
          <div>
            <div className="flex mb-2">
              <p>Tên đăng nhập:</p>
              <span className="text-red-500 ml-1">*</span>
            </div>
            <input
              defaultValue={userUpdate.userName}
              style={{ width: 560 }}
              className="w border-2 border-gray-100 rounded-lg py-1 pl-3"
              onChange={(e) => setUserName(e.target.value)}
              type="text"
              placeholder="Nhập tên đăng nhập"
            />
            <div className="flex mb-2 mt-4">
              <p>Mật khẩu:</p>
              <span className="text-red-500 ml-1">*</span>
            </div>
            <input
              defaultValue={userUpdate.password}
              style={{ width: 560 }}
              className="w border-2 border-gray-100 rounded-lg py-1 pl-3"
              onChange={(e) => setPassword(e.target.value)}
              type="text"
              placeholder="Nhập mật khẩu"
            />
            <div className="flex mb-2 mt-4">
              <p>Nhập lại mật khẩu:</p>
              <span className="text-red-500 ml-1">*</span>
            </div>
            <input
              defaultValue={userUpdate.password}
              style={{ width: 560 }}
              className="w border-2 border-gray-100 rounded-lg py-1 pl-3"
              onChange={(e) => setConfirmPassword(e.target.value)}
              type="text"
              placeholder="Nhập lại mật khẩu"
            />
            <div className="flex mb-2 mt-4">
              <p>Tình trạng</p>
              <span className="text-red-500 ml-1">*</span>
            </div>

            <select
              onChange={(e) => setRole(e.target.value)}
              value={formData.role}
              style={{ width: 560 }}
              className="py-1 border-2 border-gray-100 rounded-lg pl-3"
            >
              <option value="" className="hover:bg-orange-400">
                Tất cả
              </option>
              <option value="ngunghoatdong" className="hover:bg-orange-400">
                Ngưng hoạt động
              </option>
              <option value="hoatdong" className="hover:bg-orange-400">
                Hoạt động
              </option>
            </select>
          </div>
        </div>
      </div>
      <div className="mt-12">
        <NavLink to={"/accountmanager"}>
          <button className="bg-orange-50 border-2 border-orange-400 text-orange-400 px-14 py-3 rounded-lg mr-8">
            Hủy bỏ
          </button>
        </NavLink>
        <button
          onClick={() => {
            handleUpdateUser();
          }}
          className="bg-orange-500 text-white px-14 py-3 rounded-lg"
        >
          Cập nhật
        </button>
      </div>
    </div>
  );
}
