import React, { useEffect, useState } from "react";
import { Url_Pb } from "../../utils/Utils_Url";
import { DatePicker, DatePickerProps, Space, Table, Tooltip } from "antd";
import { NavLink, useNavigate } from "react-router-dom";
import { ColumnsType } from "antd/es/table";
import firebase from "firebase/compat/app";
import { firebaseConfig } from "../../firebase/firebase";
interface Serial {
  serial: number;
  fullName: string;
  nameService: string;
  dataTime: string;
  expiry: string;
  status: string;
  origin: string;
  action: any;
}
export default function TableSerialManager() {
  const [dataSeri, setDataSeri] = useState<Serial[]>([]);
  console.log("dataSeri", dataSeri);
  const navigate = useNavigate();
  const onChange: DatePickerProps["onChange"] = (date, dateString) => {
    console.log(date, dateString);
  };
  const columns: ColumnsType<Serial> = [
    {
      title: "STT",
      dataIndex: "serial",
      key: "serial",
      render: (text: any) => <a className="">{text}</a>,
    },
    {
      title: "Tên khách hàng",
      dataIndex: "fullName",
      key: "fullName",
    },
    {
      title: "Tên dịch vụ",
      dataIndex: "nameService",
      key: "nameService",
      //   ellipsis: {
      //     showTitle: false,
      //   },
      //   render: (address) => (
      //     <Tooltip placement="topLeft" title={address}>
      //       {address}
      //     </Tooltip>
      //   ),
    },
    {
      title: "Thời gian cấp",
      dataIndex: "dateTime",
      key: "dateTime",
    },

    {
      title: "Hạn sử dụng",
      dataIndex: "expiry",
      key: "expiry",
    },
    {
      title: "Trạng thái",
      dataIndex: "status",
      key: "status",
      render: (text: any) => {
        if (text == "Đang chờ") {
          return (
            <div className="flex items-center">
              <div className="bg-blue-500 w-2 h-2 rounded-full mr-1"></div>
              <p>Đang chờ </p>
            </div>
          );
        } else if (text == "ngunghoatdong") {
          return (
            <div className="flex items-center">
              <div className="bg-red-500 w-2 h-2 rounded-full mr-1"></div>
              <p>Ngưng hoạt động</p>
            </div>
          );
        }
      },
    },
    {
      title: "Nguồn cấp",
      dataIndex: "origin",
      key: "origin",
    },
    {
      title: " ",
      dataIndex: "action",
      key: "action",
    },
  ];
  firebase.initializeApp(firebaseConfig);
  const db = firebase.database();
  useEffect(() => {
    db.ref("serials").once("value", (snapshot) => {
      setDataSeri(snapshot.val());
    });
  }, []);
  const renderSeri = () => {
    const arrService = Object.values(dataSeri);
    return arrService.map((item: any, index) => {
      const dateTime = new Date(item.dateTime).toLocaleString("vi-VN");
      const expiry = new Date(item.expiry).toLocaleString("vi-VN");
      const ObjKey = Object.keys(dataSeri)[index];
      return {
        ...item,

        action: (
          <p
            onClick={() => {
              navigate(`/serialmanager/detail/${ObjKey}`);
            }}
            className="text-blue-500 underline cursor-pointer"
          >
            chi tiết
          </p>
        ),
        dateTime: `${dateTime}`,
        expiry: `${expiry}`,
      };
    });
  };
  return (
    <div>
      <div className="ml-6 mt-11 pr-28">
        <p className="text-orange-500 text-2xl font-bold text-left mb-4">
          Quản lý cấp số
        </p>
        <div className="flex justify-between gap-4 mb-4">
          <div>
            <p className="text-left text-base font-semibold mb-1">
              Tên dịch vụ
            </p>
            <select
              style={{ width: 154 }}
              className="py-1 border-2 border-gray-100 rounded-lg pl-3"
            >
              <option value="" className="hover:bg-orange-400">
                Tất cả
              </option>
              <option
                value="Khám sản - Phụ khoa"
                className="hover:bg-orange-400"
              >
                Khám sản - Phụ khoa
              </option>
              <option value="Khám răng hàm mặt" className="hover:bg-orange-400">
                Khám răng hàm mặt
              </option>
            </select>
          </div>
          <div>
            <p className="text-left text-base font-semibold mb-1">Tình trạng</p>
            <select
              style={{ width: 154 }}
              className="py-1 border-2 border-gray-100 rounded-lg pl-3"
            >
              <option value="" className="hover:bg-orange-400">
                Tất cả
              </option>
              <option value="Đang chờ" className="hover:bg-orange-400">
                Đang chờ
              </option>
              <option value="Đã sử dụng" className="hover:bg-orange-400">
                Đã sử dụng
              </option>
              <option value="Bỏ qua" className="hover:bg-orange-400">
                Bỏ qua
              </option>
            </select>
          </div>
          <div>
            <p className="text-left text-base font-semibold mb-1">Nguồn cấp</p>
            <select
              style={{ width: 154 }}
              className="py-1 border-2 border-gray-100 rounded-lg pl-3"
            >
              <option value="" className="hover:bg-orange-400">
                Tất cả
              </option>
              <option value="Kiosk" className="hover:bg-orange-400">
                Kiosk
              </option>
              <option value="Hệ thống" className="hover:bg-orange-400">
                Hệ thống
              </option>
            </select>
          </div>
          <div>
            <p className="text-left text-base font-semibold mb-1">
              Chọn thời gian
            </p>
            <Space direction="vertical">
              <div className="flex items-center gap-2">
                <DatePicker onChange={onChange} />
                <p className="font-bold text-sm">{">"}</p>
                <DatePicker onChange={onChange} />
              </div>
            </Space>
          </div>

          <div className="relative">
            <p className="text-left text-base font-semibold mb-1">Từ khoá</p>
            <input
              placeholder="Nhập từ khóa"
              className="py-1 border-2 border-gray-100 rounded-lg pl-3 w-72"
              type="text"
            />
            <img
              className="absolute right-3 top-9"
              src={Url_Pb + "/img/accountmanager/fi_search.png"}
              alt=""
            />
          </div>
        </div>
        <Table className="" columns={columns} dataSource={renderSeri()} />
        {/* rederUserData()  */}
        <NavLink to={"/serialmanager/addserial"}>
          <img
            style={{ top: 235 }}
            className="absolute  right-0 cursor-pointer"
            src={Url_Pb + "/img/serimanager/Component 1.png"}
            alt=""
          />
        </NavLink>
      </div>
    </div>
  );
}
