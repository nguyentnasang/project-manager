import React from "react";
import HeaderInfor from "../../component/HeaderInfor";
import TableSerialManager from "./TableSerialManager";

export default function SerialManager() {
  return (
    <div>
      <header className="flex justify-between pt-7 ml-7">
        <div className="flex text-xl text-gray-500 font-bold">
          <p className=" cursor-pointer mr-4">Cấp số</p>
          <p>{">"}</p>

          <p className=" cursor-pointer text-orange-500 ml-4">
            Danh sách cấp số
          </p>
        </div>
        <div>
          <HeaderInfor />
        </div>
      </header>
      <TableSerialManager />
    </div>
  );
}
