import {
  DatePicker,
  DatePickerProps,
  Space,
  Table,
  Tooltip,
  message,
} from "antd";
import React, { useEffect, useState } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import { Url_Pb } from "../../utils/Utils_Url";
import { ColumnsType } from "antd/es/table";
import firebase from "firebase/compat/app";
import { firebaseConfig } from "../../firebase/firebase";
interface Service {
  codeService: string;
  nameService: string;
  descService: string;
  status: string;
  detail: any;
  action: any;
}
export default function TableServiceManager() {
  const navigate = useNavigate();
  const onChange: DatePickerProps["onChange"] = (date, dateString) => {
    console.log(date, dateString);
  };
  const [dataService, setDataService] = useState<Service[]>([]);
  console.log("dataService", dataService);
  const columns: ColumnsType<Service> = [
    {
      title: "Mã dịch vụ",
      dataIndex: "codeService",
      key: "codeService",
      render: (text: any) => <a className="">{text}</a>,
    },
    {
      title: "Tên dịch vụ",
      dataIndex: "nameService",
      key: "nameService",
    },
    {
      title: "Mô tả",
      dataIndex: "descService",
      key: "descService",
      ellipsis: {
        showTitle: false,
      },
      render: (address) => (
        <Tooltip placement="topLeft" title={address}>
          {address}
        </Tooltip>
      ),
    },
    {
      title: "Trạng thái hoạt động",
      dataIndex: "status",
      key: "status",
      render: (text: any) => {
        if (text == "hoatdong") {
          return (
            <div className="flex items-center">
              <div className="bg-green-500 w-2 h-2 rounded-full mr-1"></div>
              <p>Hoạt động</p>
            </div>
          );
        } else if (text == "ngunghoatdong") {
          return (
            <div className="flex items-center">
              <div className="bg-red-500 w-2 h-2 rounded-full mr-1"></div>
              <p>Ngưng hoạt động</p>
            </div>
          );
        }
      },
    },

    {
      title: " ",
      dataIndex: "detail",
      key: "detail",
    },
    {
      title: " ",
      dataIndex: "action",
      key: "action",
    },
  ];
  firebase.initializeApp(firebaseConfig);
  const db = firebase.database();
  useEffect(() => {
    db.ref("services").once("value", (snapshot) => {
      setDataService(snapshot.val());
    });
  }, []);
  const rendderService = () => {
    const arrService = Object.values(dataService);
    return arrService.map((item, index) => {
      const ObjKey = Object.keys(dataService)[index];
      return {
        ...item,
        status: "hoatdong",
        detail: (
          <p
            onClick={() => {
              navigate(`/servicemanager/detail/${ObjKey}`);
            }}
            className="text-blue-500 underline cursor-pointer"
          >
            Chi tiết
          </p>
        ),
        action: (
          <p
            onClick={() => {
              navigate(`/servivemanager/update/${ObjKey}`);
            }}
            className="text-blue-500 underline cursor-pointer"
          >
            Cập nhật
          </p>
        ),
      };
    });
  };
  return (
    <div>
      <div className="ml-6 mt-11 pr-28">
        <p className="text-orange-500 text-2xl font-bold text-left mb-4">
          Quản lý dịch vụ
        </p>
        <div className="flex justify-between mb-4">
          <div className="flex gap-6">
            <div>
              <p className="text-left text-base font-semibold mb-1">
                Trạng thái hoạt động
              </p>
              <select
                style={{ width: 300 }}
                className="py-1 border-2 border-gray-100 rounded-lg pl-3"
              >
                <option value="" className="hover:bg-orange-400">
                  Tất cả
                </option>
                <option value="ngunghoatdong" className="hover:bg-orange-400">
                  Ngưng hoạt động
                </option>
                <option value="hoatdong" className="hover:bg-orange-400">
                  Hoạt động
                </option>
              </select>
            </div>
            <div>
              <p className="text-left text-base font-semibold mb-1">
                Chọn thời gian
              </p>
              <Space direction="vertical">
                <div className="flex items-center gap-2">
                  <DatePicker onChange={onChange} />
                  <p className="font-bold text-sm">{">"}</p>
                  <DatePicker onChange={onChange} />
                </div>
              </Space>
            </div>
          </div>
          <div className="relative">
            <p className="text-left text-base font-semibold mb-1">Từ khoá</p>
            <input
              placeholder="Nhập từ khóa"
              className="py-1 border-2 border-gray-100 rounded-lg pl-3 w-72"
              type="text"
            />
            <img
              className="absolute right-3 top-9"
              src={Url_Pb + "/img/accountmanager/fi_search.png"}
              alt=""
            />
          </div>
        </div>
        <Table className="" columns={columns} dataSource={rendderService()} />
        {/* rederUserData()  */}
        <NavLink to={"/servicemanager/add"}>
          <img
            style={{ top: 235 }}
            className="absolute  right-0 cursor-pointer"
            src={Url_Pb + "/img/servicemanager/Component 2.png"}
            alt=""
          />
        </NavLink>
      </div>
    </div>
  );
}
