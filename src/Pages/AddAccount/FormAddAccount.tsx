import { message } from "antd";
import React, { useEffect, useState } from "react";
import {
  equalTo,
  get,
  getDatabase,
  orderByChild,
  push,
  query,
  ref,
  set,
} from "firebase/database";
import "../../firebase/firebase";
import { NavLink } from "react-router-dom";
import firebase from "firebase/compat/app";
import { firebaseConfig } from "../../firebase/firebase";
export default function FormAddAccount() {
  const [fullName, setFullName] = useState("");
  const [userName, setUserName] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [role, setRole] = useState("");
  const [status, setStatus] = useState("ketoan");
  const [dataRole, setDataRole] = useState<any>([]);
  console.log("dataRole", dataRole);
  const handleAdd = async () => {
    // kiểm tra giá trị trong các ô input
    if (
      !fullName ||
      !userName ||
      !phoneNumber ||
      !password ||
      !email ||
      !confirmPassword ||
      !role ||
      !status
    ) {
      message.warning("hãy nhập đầy đủ thông tin");
      return;
    }
    //kiểm tra có nhập mật khẩu trùng nhau không
    if (password != confirmPassword) {
      message.warning("Nhập mật khẩu không trùng nhau");
      return;
    }
    // kiểm tra định dạng email
    const emailRegex = /^\S+@\S+\.\S+$/;
    if (!emailRegex.test(email)) {
      message.warning("Email không đúng định dạng");
      return;
    }

    const data = {
      fullName: fullName,
      userName: userName,
      phoneNumber: phoneNumber,
      password: password,
      email: email,
      role: role,
      status: status,
    };
    console.log(data); // in ra giá trị của các trường input

    const db = getDatabase();
    const usersRef = ref(db, "users");
    const queryRef = query(
      usersRef,
      orderByChild("userName"),
      equalTo(data.userName)
    );
    const queryEmailRef = query(
      usersRef,
      orderByChild("email"),
      equalTo(data.email)
    );

    Promise.all([get(queryRef), get(queryEmailRef)]).then((snapshots) => {
      const userNameSnapshot = snapshots[0];
      const emailSnapshot = snapshots[1];

      if (userNameSnapshot.exists()) {
        message.error("Tên đăng nhập đã tồn tại!");
      } else if (emailSnapshot.exists()) {
        message.error("Email đã tồn tại!");
      } else {
        // Thực hiện thêm mới tài khoản vào database
        const newUserRef = push(usersRef); // tạo một userId mới
        set(newUserRef, {
          fullName: data.fullName,
          userName: data.userName,
          phoneNumber: data.phoneNumber,
          password: data.password,
          email: data.email,
          role: data.role,
          status: data.status,
        })
          .then(() => {
            message.success("Thêm mới tài khoản thành công!");
          })
          .catch((error) => {
            message.error("Lỗi khi thêm mới tài khoản: " + error);
          });
      }
    });

    // ............................................................................
  };
  firebase.initializeApp(firebaseConfig);
  const Database = firebase.database();

  useEffect(() => {
    Database.ref("roles")
      .once("value")
      .then((res) => {
        setDataRole(res.val());
      })
      .catch((err) => {});
  }, []);
  const arrRole = Object?.values(dataRole);
  console.log("arrRole", arrRole);
  const renderOptionRole = arrRole?.map((item: any) => (
    <option className="" value={item.nameRole} key={item.nameRole}>
      {item.nameRole}
    </option>
  ));
  return (
    <div
      style={{ height: 534 }}
      className="bg-white rounded-lg px-6 pt-4 mt-2 mb-32 relative"
    >
      <span className="text-orange-500 font-bold text-xl">
        Thông tin tài khoản
      </span>
      <div className="grid grid-cols-2 text-base mt-5">
        <div>
          <span className="flex items-center mb-2 mt-4">
            <p>Họ tên</p>
            <p className="text-red-500 ml-1">*</p>
          </span>
          <input
            style={{ width: 560 }}
            className="w border-2 border-gray-100 rounded-lg py-1 pl-3"
            onChange={(e) => setFullName(e.target.value)}
            type="text"
            placeholder="Nhập họ tên"
          />
          <span className="flex items-center mb-2 mt-4">
            <p>Số điện thoại</p>
            <p className="text-red-500 ml-1">*</p>
          </span>
          <input
            style={{ width: 560 }}
            className="w border-2 border-gray-100 rounded-lg py-1 pl-3"
            onChange={(e) => setPhoneNumber(e.target.value)}
            type="text"
            placeholder="Nhập số điện thoại"
          />
          <span className="flex items-center mb-2 mt-4">
            <p>Email</p>
            <p className="text-red-500 ml-1">*</p>
          </span>
          <input
            style={{ width: 560 }}
            className="w border-2 border-gray-100 rounded-lg py-1 pl-3"
            onChange={(e) => setEmail(e.target.value)}
            type="text"
            placeholder="Nhập email"
          />
          <span className="flex items-center mb-2 mt-4">
            <p>Vai trò</p>
            <p className="text-red-500 ml-1">*</p>
          </span>
          <select
            style={{ width: 560 }}
            className="py-1 border-2 border-gray-100 rounded-lg pl-3"
            onChange={(e) => setRole(e.target.value)}
          >
            <option value="" className="hover:bg-orange-400">
              Tất cả
            </option>
            <option value="ngunghoatdong" className="hover:bg-orange-400">
              Ngưng hoạt động
            </option>
            <option value="hoatdong" className="hover:bg-orange-400">
              Hoạt động
            </option>
          </select>
          <span className="flex items-center mt-4 text-sm font-normal ">
            <p className="text-red-500 ml-1 mr-1">*</p>
            <p className="text-gray-400">Là trường thông tin bắt buộc</p>
          </span>
        </div>
        <div>
          <span className="flex items-center mb-2 mt-4">
            <p>Tên đăng nhập:</p>
            <p className="text-red-500 ml-1">*</p>
          </span>
          <input
            style={{ width: 560 }}
            className="w border-2 border-gray-100 rounded-lg py-1 pl-3"
            onChange={(e) => setUserName(e.target.value)}
            type="text"
            placeholder="Nhập tên đăng nhập"
          />

          <span className="flex items-center mb-2 mt-4">
            <p>Mật khẩu:</p>
            <p className="text-red-500 ml-1">*</p>
          </span>
          <input
            style={{ width: 560 }}
            className="w border-2 border-gray-100 rounded-lg py-1 pl-3"
            onChange={(e) => setPassword(e.target.value)}
            type="text"
            placeholder="Nhập mật khẩu"
          />

          <span className="flex items-center mb-2 mt-4">
            <p>Nhập lại mật khẩu</p>
            <p className="text-red-500 ml-1">*</p>
          </span>
          <input
            style={{ width: 560 }}
            className="w border-2 border-gray-100 rounded-lg py-1 pl-3"
            onChange={(e) => setConfirmPassword(e.target.value)}
            type="text"
            placeholder="Nhập lại mật khẩu"
          />

          <span className="flex items-center mb-2 mt-4">
            <p>Tình trạng</p>
            <p className="text-red-500 ml-1">*</p>
          </span>

          <select
            style={{ width: 560 }}
            className="py-1 border-2 border-gray-100 rounded-lg pl-3"
            onChange={(e) => setStatus(e.target.value)}
          >
            {renderOptionRole}
          </select>
        </div>
      </div>

      <br />
      <div
        style={{ bottom: -80 }}
        className="flex justify-center w-full absolute"
      >
        <NavLink to={"/accountmanager"}>
          <button className="bg-orange-50 border-2 border-orange-400 text-orange-400 px-14 py-3 rounded-lg mr-8">
            Hủy bỏ
          </button>
        </NavLink>
        <button
          onClick={() => {
            handleAdd();
          }}
          className="bg-orange-500 text-white px-14 py-3 rounded-lg"
        >
          Thêm
        </button>
      </div>
    </div>
  );
}
